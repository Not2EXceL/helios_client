package com.not2excel.helios.event.player;

import com.not2excel.lib.event.CancellableEvent;
import net.minecraft.src.Entity;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/2/13
 */
public class EventPreAttack extends CancellableEvent
{
    private Entity entity;

    public EventPreAttack(final Entity entity)
    {
        this.entity = entity;
    }

    public Entity getEntity()
    {
        return entity;
    }
}
