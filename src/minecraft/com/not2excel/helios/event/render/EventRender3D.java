package com.not2excel.helios.event.render;

import com.not2excel.lib.event.Event;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/1/13
 */
public class EventRender3D extends Event
{
    private final float tick;

    public EventRender3D(final float tick)
    {
        this.tick = tick;
    }

    public float getTick()
    {
        return tick;
    }

    public double estimatePosition(double previous, double current)
    {
        return previous + (current - previous) * (double)this.tick;
    }
}
