package com.not2excel.helios.event.chat;

import com.not2excel.lib.event.CancellableEvent;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class EventChatSend extends CancellableEvent
{
    private String message;

    public EventChatSend(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(final String message)
    {
        this.message = message;
    }
}
