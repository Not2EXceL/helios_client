package com.not2excel.helios.event.client;

import com.not2excel.lib.event.CancellableEvent;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/20/13
 */
public class EventKeyPressed extends CancellableEvent
{
    private int key;

    public EventKeyPressed(final int key)
    {
        this.key = key;
    }

    public int getKey()
    {
        return key;
    }
}
