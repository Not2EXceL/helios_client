package com.not2excel.helios.event.client;

import com.not2excel.lib.event.CancellableEvent;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/2/13
 */
public class EventClick extends CancellableEvent
{
    private int button;

    public EventClick(int button)
    {
        this.button = button;
    }

    public int getButton()
    {
        return button;
    }
}
