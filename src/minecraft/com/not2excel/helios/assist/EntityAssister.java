package com.not2excel.helios.assist;

import net.minecraft.src.Entity;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/6/13
 */
public class EntityAssister
{
    private static volatile EntityAssister instance;

    public static EntityAssister getInstance()
    {
        if (instance == null)
        { instance = new EntityAssister(); }
        return instance;
    }

    public double entityPosition(final Entity entity, final float ticks, final int xyz)
    {
        if (xyz == 0)
        { return entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * ticks; }
        if (xyz == 1)
        {
            return entity.lastTickPosY + entity.getEyeHeight() / 2 +
                   (entity.posY - entity.lastTickPosY) * ticks;
        }
        if (xyz == 2)
        { return entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * ticks; }
        return -1;
    }
}
