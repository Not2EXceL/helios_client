package com.not2excel.helios.assist;

import com.not2excel.helios.client.Wrapper;
import net.minecraft.src.MathHelper;
import org.lwjgl.util.vector.Vector3f;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/2/13
 */
public class BlockAssister
{
    private static volatile BlockAssister instance;

    public static BlockAssister getInstance()
    {
        if (instance == null)
        { instance = new BlockAssister(); }
        return instance;
    }

    public synchronized boolean isBlockReachable(final Vector3f vector, final double distance)
    {
        if (vector == null)
        { return false; }
        return PlayerAssister.getInstance().distanceToCoords(vector.getX(), vector.getY(),
                                                             vector.getZ()) < distance &&
               PlayerAssister.getInstance().distanceToCoords(vector.getX(), vector.getY(),
                                                             vector.getZ()) > -distance;
    }

    public int getBlockID(final Vector3f vector)
    {
        if (vector == null)
        { return -1; }
        return Wrapper.getInstance().getWorld().getBlockId((int) vector.getX(), (int) vector.getY(),
                                                           (int) vector.getZ());
    }

    public void faceBlock(final double blockX, final double blockY, final double blockZ)
    {
        final double relX = blockX - Wrapper.getInstance().getPlayer().posX;
        final double relY = blockY -
                            (Wrapper.getInstance().getPlayer().posY + Wrapper.getInstance().getPlayer().getEyeHeight());
        final double relZ = blockZ - Wrapper.getInstance().getPlayer().posZ;
        final double dist = MathHelper.sqrt_double(relX * relX + relZ * relZ);
        final float yaw = (float) ((Math.atan2(relZ, relX) * 180.0D / Math.PI) - 90.0F);
        final float pitch = (float) -(Math.atan2(relY, dist) * 180.0D / Math.PI);
        Wrapper.getInstance().getPlayer().rotationYaw = yaw;
        Wrapper.getInstance().getPlayer().rotationPitch = pitch;
    }
}
