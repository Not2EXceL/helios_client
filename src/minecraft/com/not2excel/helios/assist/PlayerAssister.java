package com.not2excel.helios.assist;

import com.not2excel.helios.client.Wrapper;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.MathHelper;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/31/13
 */
public class PlayerAssister
{
    private static volatile PlayerAssister instance;

    public static PlayerAssister getInstance()
    {
        if (instance == null)
        { instance = new PlayerAssister(); }
        return instance;
    }

    public double distanceToCoords(final double x, final double y, final double z)
    {
        EntityClientPlayerMP player = Wrapper.getInstance().getPlayer();
        double xDiff = player.posX - x;
        double yDiff = player.posY - y;
        double zDiff = player.posZ - z;
        return MathHelper.sqrt_double(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
    }
}
