package com.not2excel.helios.logger;

import com.not2excel.helios.client.Utilities;
import com.not2excel.helios.client.Variables;
import com.not2excel.helios.util.color.ColorHelper;
import com.not2excel.lib.logger.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class HeliosLogger extends Logger
{
    private static volatile HeliosLogger instance;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
    private final Calendar         calendar         = Calendar.getInstance();

    public static HeliosLogger getInstance()
    {
        if (instance == null)
        { instance = new HeliosLogger(); }
        return instance;
    }

    public void logChat(final String type, final Object data)
    {
        Utilities.getInstance().addMessage(String.format("[%s][%s]: %s",
                                                         ColorHelper.PURPLE +
                                                         Variables.getInstance().CLIENT_TITLE +
                                                         ColorHelper.WHITE,
                                                         type, data.toString())
                                                 .replace("[", ColorHelper.DARK_GREY + "[")
                                                 .replace("]", ColorHelper.DARK_GREY + "]" + ColorHelper.WHITE));
        logConsole(type, data);
    }

    @Override
    public void logConsole(final String type, final Object data)
    {
        System.out.println(ColorHelper.stripColorCodes(String.format("[%s][%s][%s]: %s", getCurrentTime(),
                                                                     Variables.getInstance().CLIENT_TITLE,
                                                                     type, data.toString())
                                                             .replace("[", ColorHelper.DARK_GREY + "[")
                                                             .replace("]",
                                                                      ColorHelper.DARK_GREY + "]" + ColorHelper.WHITE)));
    }

    private String getCurrentTime()
    {
        return simpleDateFormat.format(calendar.getTime());
    }
}
