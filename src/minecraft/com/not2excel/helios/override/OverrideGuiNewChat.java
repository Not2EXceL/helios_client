package com.not2excel.helios.override;

import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.util.betterfonts.FontHandler;
import com.not2excel.helios.util.drawing.Drawing2D;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/26/13
 */
public class OverrideGuiNewChat extends GuiNewChat
{
    public int dragX = 0, dragY = 0, lastDragX = 0, lastDragY = 0;
    private int[] position = new int[2];
    private int   startY   = 0;
    private int   width    = 22;

    public OverrideGuiNewChat(Minecraft par1Minecraft)
    {
        super(par1Minecraft);
    }

    @Override
    public void drawChat(int par1)
    {
        if (!FontHandler.getInstance().isGlobalTTF())
        { super.drawChat(par1); }
        if (this.mc.gameSettings.chatVisibility != 2)
        {
            int var2 = this.func_96127_i();
            boolean var3 = false;
            int var4 = 0;
            int var5 = this.field_96134_d.size();
            float var6 = this.mc.gameSettings.chatOpacity * 0.9F + 0.1F;
            startY = 0;
            width = Wrapper.getInstance().getFontRenderer().getStringWidth("Chat");

            if (var5 > 0)
            {
                if (this.getChatOpen())
                {
                    var3 = true;
                }

                float var7 = this.func_96131_h();
                int var8 = MathHelper.ceiling_float_int((float) this.func_96126_f() / var7);
                GL11.glPushMatrix();
                GL11.glTranslatef(2.0F, 20.0F, 0.0F);
                GL11.glScalef(var7, var7, 1.0F);
                int var9;
                int var11;
                int var14;

                for (var9 = 0; var9 + this.field_73768_d < this.field_96134_d.size() && var9 < var2; ++var9)
                {
                    ChatLine var10 = (ChatLine) this.field_96134_d.get(var9 + this.field_73768_d);

                    if (var10 != null)
                    {
                        var11 = par1 - var10.getUpdatedCounter();

                        if (var11 < 200 || var3)
                        {
                            double var12 = (double) var11 / 200.0D;
                            var12 = 1.0D - var12;
                            var12 *= 10.0D;

                            if (var12 < 0.0D)
                            {
                                var12 = 0.0D;
                            }

                            if (var12 > 1.0D)
                            {
                                var12 = 1.0D;
                            }

                            var12 *= var12;
                            var14 = (int) (255.0D * var12);

                            if (var3)
                            {
                                var14 = 255;
                            }

                            var14 = (int) ((float) var14 * var6);
                            ++var4;

                            if (var14 > 3)
                            {
                                GL11.glEnable(GL11.GL_BLEND);
                                startY -=
                                        Wrapper.getInstance().getFontRenderer().stringCache.getStringHeight() + 2;
                                if (width < Wrapper.getInstance().getFontRenderer().getStringWidth(
                                        var10.getChatLineString()))
                                {
                                    width = Wrapper.getInstance().getFontRenderer().getStringWidth(
                                            var10.getChatLineString());
                                }
                            }
                        }
                    }
                }

                if (var4 > 0)
                {
                    byte var15 = 0;
                    int height = 0;
                    height = Wrapper.getInstance().getFontRenderer().stringCache.getStringHeight() + 4;
                    Drawing2D.drawBorderedRect(3 + dragX - 0.5f, startY - height / 4 - height + dragY - 0.5f,
                                               width + 9 + dragX + 0.5f,
                                               height / 6 + dragY + 0.25f,
                                               0xff3c3c3c,
                                               0x00ffffff);
                    Drawing2D.drawBorderedRect(3 + dragX, startY - height / 4 - height + dragY,
                                               width + 9 + dragX,
                                               height / 6 + dragY,
                                               0x80ffffff,
                                               0x7F000000);
                    Drawing2D.drawBorderedRect(6 + dragX - 0.5f, startY - height / 4 + dragY - 0.5f,
                                               width + 6 + dragX + 0.5f, startY - height / 4 + dragY + 0.5f,
                                               0xff3c3c3c,
                                               0x00ffffff);
                    Drawing2D.drawBorderedRect(6 + dragX, startY - height / 4 + dragY, width + 6 + dragX,
                                               startY - height / 4 + dragY,
                                               0x80ffffff,
                                               0x90ffffff);
                    Wrapper.getInstance().getFontRenderer().drawStringWithShadow("Chat", 5 + dragX,
                                                                                 startY - height / 4 - 9 + dragY,
                                                                                 0xFFffffff);
                }

                for (var9 = 0; var9 + this.field_73768_d < this.field_96134_d.size() && var9 < var2; ++var9)
                {
                    ChatLine var10 = (ChatLine) this.field_96134_d.get(var9 + this.field_73768_d);

                    if (var10 != null)
                    {
                        var11 = par1 - var10.getUpdatedCounter();

                        if (var11 < 200 || var3)
                        {
                            double var12 = (double) var11 / 200.0D;
                            var12 = 1.0D - var12;
                            var12 *= 10.0D;

                            if (var12 < 0.0D)
                            {
                                var12 = 0.0D;
                            }

                            if (var12 > 1.0D)
                            {
                                var12 = 1.0D;
                            }

                            var12 *= var12;
                            var14 = (int) (255.0D * var12);

                            if (var3)
                            {
                                var14 = 255;
                            }

                            var14 = (int) ((float) var14 * var6);
                            ++var4;

                            if (var14 > 3)
                            {
                                GL11.glEnable(GL11.GL_BLEND);
                                int height = 0;
                                height =
                                        Wrapper.getInstance().getFontRenderer().stringCache.getStringHeight() + 2;
                                int var16 = -var9 * height;
                                String var17 = var10.getChatLineString();

                                if (!this.mc.gameSettings.chatColours)
                                {
                                    var17 = StringUtils.stripControlCodes(var17);
                                }
                                Wrapper.getInstance().getFontRenderer().drawStringWithShadow(var17, 5 + dragX,
                                                                                             var16 - height +
                                                                                             dragY,
                                                                                             16777215 +
                                                                                             (var14 << 24));

                            }
                        }
                    }
                }

                GL11.glPopMatrix();
            }
        }
    }

    @Override
    public void mouseClicked(int x, int y, int b)
    {
        ScaledResolution sr = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
        position[0] = x;
        position[1] = y;
        if (b == 0)
        {
            int height = 0;
            height = Wrapper.getInstance().getFontRenderer().stringCache.getStringHeight() + 4;
            if (x >= 3 + dragX && y >= startY - height / 1 + sr.getScaledHeight() - 36 + dragY && x <= width + dragX &&
                y <= startY + height / 2 + sr.getScaledHeight() - 36 + dragY)
            {
                lastDragX = x - dragX;
                lastDragY = y - dragY;
                dragging = true;
                System.out.println("dragging");
            }
            System.out.println("x:" + x + "y:" + y);
        }
    }

    public void dragChat(int x, int y)
    {
        dragX = x - lastDragX;
        dragY = y - lastDragY;
    }

    @Override
    public void mouseMovedOrUp(int x, int y, int b)
    {
        position[0] = x;
        position[1] = y;
        if (b == 0)
        { dragging = false; }
    }
}