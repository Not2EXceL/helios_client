package com.not2excel.helios.override;

import com.not2excel.helios.client.Variables;
import com.not2excel.helios.command.CommandLoader;
import com.not2excel.helios.event.chat.EventChatSend;
import com.not2excel.helios.event.player.EventPostMotionUpdate;
import com.not2excel.helios.event.player.EventPostUpdate;
import com.not2excel.helios.event.player.EventPreMotionUpdate;
import com.not2excel.helios.event.player.EventPreUpdate;
import com.not2excel.lib.event.EventManager;
import net.minecraft.src.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class OverrideEntityClientPlayerMP extends EntityClientPlayerMP
{
    public OverrideEntityClientPlayerMP(Minecraft par1Minecraft, World par2World,
                                        Session par3Session,
                                        NetClientHandler par4NetClientHandler)
    {
        super(par1Minecraft, par2World, par3Session, par4NetClientHandler);
    }

    @Override
    public void onUpdate()
    {
        EventPreUpdate eventPreUpdate = new EventPreUpdate();
        EventManager.getInstance().fireEvent(eventPreUpdate);
        if (eventPreUpdate.isCancelled())
        { return; }
        super.onUpdate();
        EventManager.getInstance().fireEvent(new EventPostUpdate());
    }

    @Override
    public void sendMotionUpdates()
    {
        EventPreMotionUpdate eventPreMotionUpdate = new EventPreMotionUpdate();
        EventManager.getInstance().fireEvent(eventPreMotionUpdate);
        if (eventPreMotionUpdate.isCancelled())
        { return; }
        super.sendMotionUpdates();
        EventManager.getInstance().fireEvent(new EventPostMotionUpdate());

    }

    @Override
    public void sendChatMessage(String par1Str)
    {
        EventChatSend eventChatSend = new EventChatSend(par1Str);
        EventManager.getInstance().fireEvent(eventChatSend);
        if (eventChatSend.isCancelled())
        { return; }
        if (par1Str.startsWith(Variables.getInstance().CHAT_PREFIX))
        { CommandLoader.getInstance().runCommands(par1Str); }
        else
        { super.sendChatMessage(par1Str); }
    }
}
