package com.not2excel.helios.override;

import net.minecraft.src.EntityRenderer;
import net.minecraft.src.Minecraft;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/1/13
 */
public class OverrideEntityRenderer extends EntityRenderer
{
    public OverrideEntityRenderer(Minecraft par1Minecraft)
    {
        super(par1Minecraft);
    }
}
