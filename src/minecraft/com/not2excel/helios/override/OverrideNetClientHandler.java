package com.not2excel.helios.override;

import com.google.common.base.Charsets;
import net.minecraft.client.ClientBrandRetriever;
import net.minecraft.src.*;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class OverrideNetClientHandler extends NetClientHandler
{
    public OverrideNetClientHandler(Minecraft par1Minecraft, String par2Str, int par3) throws IOException
    {
        super(par1Minecraft, par2Str, par3);
    }

    public OverrideNetClientHandler(Minecraft par1Minecraft, String par2Str, int par3,
                                    GuiScreen par4GuiScreen) throws IOException
    {
        super(par1Minecraft, par2Str, par3, par4GuiScreen);
    }

    public OverrideNetClientHandler(Minecraft par1Minecraft, IntegratedServer par2IntegratedServer)
            throws IOException
    {
        super(par1Minecraft, par2IntegratedServer);
    }

    @Override
    public void handleLogin(Packet1Login par1Packet1Login)
    {
        this.mc.playerController = new OverridePlayerControllerMP(this.mc, this);
        this.mc.statFileWriter.readStat(StatList.joinMultiplayerStat, 1);
        this.worldClient = new WorldClient(this, new WorldSettings(0L, par1Packet1Login.gameType, false, par1Packet1Login.hardcoreMode, par1Packet1Login.terrainType), par1Packet1Login.dimension, par1Packet1Login.difficultySetting, this.mc.mcProfiler, this.mc.getLogAgent());
        this.worldClient.isRemote = true;
        this.mc.loadWorld(this.worldClient);
        this.mc.thePlayer.dimension = par1Packet1Login.dimension;
        this.mc.displayGuiScreen(new GuiDownloadTerrain(this));
        this.mc.thePlayer.entityId = par1Packet1Login.clientEntityId;
        this.currentServerMaxPlayers = par1Packet1Login.maxPlayers;
        this.mc.playerController.setGameType(par1Packet1Login.gameType);
        this.mc.gameSettings.sendSettingsToServer();
        this.netManager.addToSendQueue(new Packet250CustomPayload("MC|Brand", ClientBrandRetriever.getClientModName().getBytes(
                Charsets.UTF_8)));
    }
}
