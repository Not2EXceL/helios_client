package com.not2excel.helios.override;

import com.not2excel.helios.event.player.EventPostAttack;
import com.not2excel.helios.event.player.EventPreAttack;
import com.not2excel.lib.event.EventManager;
import net.minecraft.src.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class OverridePlayerControllerMP extends PlayerControllerMP
{
    public OverridePlayerControllerMP(Minecraft par1Minecraft,
                                      NetClientHandler par2NetClientHandler)
    {
        super(par1Minecraft, par2NetClientHandler);
    }

    @Override
    public EntityClientPlayerMP func_78754_a(World par1World)
    {
        return new OverrideEntityClientPlayerMP(this.mc, par1World, this.mc.func_110432_I(), this.netClientHandler);
    }

    @Override
    public void attackEntity(EntityPlayer par1EntityPlayer, Entity par2Entity)
    {
        EventPreAttack eventPreAttack = new EventPreAttack(par2Entity);
        EventManager.getInstance().fireEvent(eventPreAttack);
        super.attackEntity(par1EntityPlayer, par2Entity);
        EventManager.getInstance().fireEvent(new EventPostAttack());
    }
}
