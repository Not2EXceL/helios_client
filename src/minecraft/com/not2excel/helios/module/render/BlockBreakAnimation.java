package com.not2excel.helios.module.render;

import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.event.render.EventRender3D;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.helios.module.ModuleType;
import com.not2excel.helios.util.drawing.Drawing3D;
import com.not2excel.lib.event.EventHandler;
import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.MovingObjectPosition;
import net.minecraft.src.RenderManager;
import org.lwjgl.opengl.GL11;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/1/13
 */
public class BlockBreakAnimation extends ModuleBase
{
    public BlockBreakAnimation()
    {
        super("Block Break Animation", "Not2EXceL", "Renders nice block breaking effect.", ModuleType.RENDER);
        setStatus(true);
        setNotArraylistRenderable(true);
    }

    @EventHandler(event = EventRender3D.class)
    public void render3D()
    {
        MovingObjectPosition objectMouseOver = Wrapper.getInstance().getMinecraft().objectMouseOver;
        float curBlockDamageMP = Wrapper.getInstance().getPlayerController().curBlockDamageMP;
        if (objectMouseOver == null ||
            curBlockDamageMP <= 0.0)
        { return; }
        double x = objectMouseOver.blockX - RenderManager.renderPosX;
        double y = objectMouseOver.blockY - RenderManager.renderPosY;
        double z = objectMouseOver.blockZ - RenderManager.renderPosZ;
        GL11.glTranslated(x, y, z);
        Drawing3D.renderColoredOutlinedBoundingBox(new AxisAlignedBB(0, 0, 0, 1,
                                                                     (curBlockDamageMP > 0.5 ? 1 - curBlockDamageMP :
                                                                      curBlockDamageMP),
                                                                     1), 1 - curBlockDamageMP, curBlockDamageMP, 0,
                                                   1.6f, true);
        Drawing3D.renderColoredOutlinedBoundingBox(new AxisAlignedBB(0, 1, 0, 1, 1 -
                                                                                 (curBlockDamageMP > 0.5 ?
                                                                                  1 - curBlockDamageMP :
                                                                                  curBlockDamageMP),
                                                                     1), 1 - curBlockDamageMP, curBlockDamageMP, 0,
                                                   1.6f, true);
        GL11.glTranslated(0.5, 0, 0.5);
        GL11.glRotated((curBlockDamageMP > 0.5 ? -curBlockDamageMP : curBlockDamageMP) * 990, 0, 1, 0);
        GL11.glTranslated(-0.5, 0, -0.5);
        Drawing3D.renderColoredOutlinedBoundingBox(new AxisAlignedBB(curBlockDamageMP, curBlockDamageMP,
                                                                     curBlockDamageMP,
                                                                     1 - curBlockDamageMP, 1 - curBlockDamageMP,
                                                                     1 - curBlockDamageMP),
                                                   1 - curBlockDamageMP, curBlockDamageMP, 0, 1.6f, true);
    }

    @Override
    public String[] getAliases()
    {
        return new String[]{"blockbreak"};
    }

    @Override
    public String getSyntax()
    {
        return "";
    }
}
