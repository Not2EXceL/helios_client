package com.not2excel.helios.module.render;

import com.not2excel.helios.client.Utilities;
import com.not2excel.helios.event.render.EventRenderGui;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.helios.module.ModuleType;
import com.not2excel.helios.util.drawing.Drawing2D;
import com.not2excel.lib.event.EventHandler;
import com.not2excel.lib.persistence.integer.IncrementingInteger;
import com.not2excel.lib.time.TimeManager;
import org.lwjgl.opengl.GL11;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/5/13
 */
public class MorganFreeman extends ModuleBase
{
    private IncrementingInteger x      = new IncrementingInteger(Utilities.getInstance().getScaledWidth() / 2 - 64);
    private IncrementingInteger y      = new IncrementingInteger(Utilities.getInstance().getScaledHeight() / 2 - 64);
    private int                 width  = Utilities.getInstance().getScaledWidth();
    private int                 height = Utilities.getInstance().getScaledHeight();
    private TimeManager timeManager;

    public MorganFreeman()
    {
        super("Morgan Freeman", "Not2EXceL", "You don't need to know anything more than Morgan Freeman.",
              ModuleType.RENDER);
        timeManager = new TimeManager();
        x.setIncrement(20);
        y.setIncrement(20);
    }

    @Override
    public void onToggle(boolean status)
    {
        if (status)
        { timeManager.resetLastTime(); }
    }

    @EventHandler(event = EventRenderGui.class)
    public void renderGui()
    {
        if (timeManager.sleepMillis(100))
        {
            x.setIncrement(50);
            y.setIncrement(50);
            x.incrementInt();
            y.incrementInt();
            timeManager.resetLastTime();
        }
        if (x.getInt() > width)
        { x = new IncrementingInteger(0); }
        if (x.getInt() < -128)
        { x = new IncrementingInteger(width); }
        if (y.getInt() > height)
        { y = new IncrementingInteger(0); }
        if (y.getInt() < -128)
        { y = new IncrementingInteger(height); }
        GL11.glScaled(0.75, 0.75, 0.75);
        Drawing2D.drawImage(x.getInt(), y.getInt(), 128, 128, "helios/morganfreeman.png");
        GL11.glScaled(1 / 0.75, 1 / 0.75, 1 / 0.75);
    }


    @Override
    public String[] getAliases()
    {
        return new String[]{"morganfreeman", "morgan"};
    }

    @Override
    public String getSyntax()
    {
        return "";
    }
}
