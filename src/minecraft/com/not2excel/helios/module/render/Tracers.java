package com.not2excel.helios.module.render;

import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.event.render.EventRender3D;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.helios.module.ModuleType;
import com.not2excel.helios.util.drawing.GL11Assist;
import com.not2excel.lib.command.Command;
import com.not2excel.lib.event.EventHandler;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.EntityLivingBase;
import net.minecraft.src.RenderManager;
import org.lwjgl.opengl.GL11;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/21/13
 */
public class Tracers extends ModuleBase implements Command
{
    private boolean traceAll     = true;
    private boolean tracePlayers = false;
    private boolean traceMobs    = false;
    private boolean traceAnimals = false;

    public Tracers()
    {
        super("Tracers", "Not2EXceL", "Renders lines to other players", ModuleType.RENDER);
    }

    @EventHandler(event = EventRender3D.class)
    public void render3d(EventRender3D event)
    {
        for (Object object : Wrapper.getInstance().getWorld().playerEntities)
        {
            EntityClientPlayerMP player = Wrapper.getInstance().getPlayer();
            EntityLivingBase entity = (EntityLivingBase) object;
            if (entity.isDead || entity == player)
            { continue; }
            double renderX = event.estimatePosition(entity.prevPosX, entity.posX) - RenderManager.renderPosX;
            double renderY = event.estimatePosition(entity.prevPosY, entity.posY) - RenderManager.renderPosY;
            double renderZ = event.estimatePosition(entity.prevPosZ, entity.posZ) - RenderManager.renderPosZ;

            GL11Assist.defaultsOn3D(false);
            GL11.glColor3d(1, 1, 0);
            GL11.glBegin(GL11.GL_LINE_STRIP);
            GL11.glVertex3d(0, 0, 0);
            GL11.glVertex3d(renderX, renderY, renderZ);
            GL11.glEnd();
            GL11Assist.defaultsOff3D();
        }
    }

    @Override
    public String[] getAliases()
    {
        return new String[]{"tracers"};
    }

    @Override
    public String getSyntax()
    {
        return "-trace <all/players/mobs/animals>";
    }

    @Override
    public void onCommand(String fullCommand, String[] args)
    {
    }
}
