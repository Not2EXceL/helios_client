package com.not2excel.helios.module.render;

import com.not2excel.helios.assist.EntityAssister;
import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.event.render.EventRender3D;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.helios.module.ModuleType;
import com.not2excel.helios.util.drawing.GL11Assist;
import com.not2excel.lib.event.EventHandler;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.RenderManager;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.glu.Sphere;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/6/13
 */
public class HitSpheres extends ModuleBase
{
    private Sphere sphere = new Sphere();

    public HitSpheres()
    {
        super("Hit Spheres", "Not2EXceL", "Hit Spheres around players", ModuleType.RENDER);
        sphere.setDrawStyle(GLU.GLU_SILHOUETTE);
    }

    @EventHandler(event = EventRender3D.class)
    public void render3D(EventRender3D event)
    {
        GL11Assist.defaultsOn3D(true);
        GL11.glColor4d(1, 0.2, 0, 0.66);
        for (Object object : Wrapper.getInstance().getWorld().loadedEntityList)
        {
            if (!(object instanceof Entity))
            { continue; }
            Entity entity = (Entity) object;
            if (!(entity instanceof EntityPlayer) || entity == Wrapper.getInstance().getPlayer())
            { continue; }
            double renderX = EntityAssister.getInstance().entityPosition(entity, event.getTick(), 0) -
                             RenderManager.renderPosX;
            double renderY = EntityAssister.getInstance().entityPosition(entity, event.getTick(), 1) -
                             RenderManager.renderPosY;
            double renderZ = EntityAssister.getInstance().entityPosition(entity, event.getTick(), 2) -
                             RenderManager.renderPosZ;
            GL11.glPushMatrix();
            GL11.glTranslated(renderX, renderY, renderZ);
            sphere.draw(4, 30, 30);
            GL11.glPopMatrix();
        }
        GL11Assist.defaultsOff3D();
    }

    @Override
    public String[] getAliases()
    {
        return new String[]{"hitspheres"};
    }

    @Override
    public String getSyntax()
    {
        return "";
    }
}
