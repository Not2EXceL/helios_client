package com.not2excel.helios.module.render;

import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.event.render.EventRenderGui;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.helios.module.ModuleType;
import com.not2excel.lib.event.EventHandler;
import com.not2excel.lib.module.Module;
import com.not2excel.lib.module.ModuleManager;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/16/13
 */
public class ArrayList extends ModuleBase
{
    public ArrayList()
    {
        super("Arraylist", "Not2EXceL", "Renders currently enabled mods in game", ModuleType.NONE);
        setStatus(true);
        setNotArraylistRenderable(true);
    }

    @EventHandler(event = EventRenderGui.class)
    public void renderArrayList()
    {
        final List<Module> enabledModules = new LinkedList<Module>();
        for (final Module module : ModuleManager.getInstance().getModuleArray())
        {
            if (module.getStatus() && !((ModuleBase)module).isNotArraylistRenderable())
            { enabledModules.add(module); }
        }
        if (enabledModules.isEmpty())
        { return; }
        int i = 0;
        for (final Module module : enabledModules)
        {
            final String displayName = module.getLabel();
            Wrapper.getInstance().getFontRenderer().drawStringWithShadow(displayName,
                                                                         Wrapper.getInstance()
                                                                                .getMinecraft().displayWidth / 2 -
                                                                         Wrapper.getInstance().getFontRenderer()
                                                                                .getStringWidth(displayName) - 2,
                                                                         2 + i * (Wrapper.getInstance().getFontRenderer()
                                                                                 .stringCache.getStringHeight() + 2),
                                                                         ((ModuleBase) module).getColor());
            i++;
        }
    }

    @Override
    public String[] getAliases()
    {
        return new String[]{"arraylist"};
    }

    @Override
    public String getSyntax()
    {
        return "";
    }
}
