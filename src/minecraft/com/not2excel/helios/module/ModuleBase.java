package com.not2excel.helios.module;

import com.not2excel.helios.logger.HeliosLogger;
import com.not2excel.helios.util.color.ColorHelper;
import com.not2excel.lib.module.Module;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/22/13
 */
public abstract class ModuleBase extends Module
{
    private int        keybind                = -1;
    private int        color                  = 0xFFffffff;
    private ModuleType type                   = ModuleType.NONE;
    private boolean    notToggleable          = false;
    private boolean    notArraylistRenderable = false;

    public ModuleBase(final String l)
    {
        this(l, null);
    }

    public ModuleBase(final String l, final String a)
    {
        this(l, a, null);
    }

    public ModuleBase(final String l, final String a, final String d)
    {
        this(l, a, d, ModuleType.NONE);
    }

    public ModuleBase(final String l, final String a, final String d, ModuleType type)
    {
        this(l, a, d, type, -1, 0xFFffffff);
    }

    public ModuleBase(final String l, final String a, final String d, final ModuleType mt, int keybind, int color)
    {
        super(l, a, d);
        setType(mt);
        setKeybind(keybind);
        setColor(color);
    }

    public abstract String[] getAliases();

    public abstract String getSyntax();

    public boolean isToggleable()
    {
        if (notToggleable)
        { return false; }
        return (getType() != ModuleType.NONE) && (getType() != ModuleType.COMMAND);
    }

    public void toggleModule(final boolean chat)
    {
        if (!isToggleable())
        { return; }
        setStatus(!getStatus());
        if (chat)
        {
            HeliosLogger.getInstance().logChat("Module",
                                               String.format("%s toggled %s",
                                                             ColorHelper.GOLD + getLabel() + ColorHelper.WHITE,
                                                             getStatus() ? ColorHelper.DARK_GREEN + "on" :
                                                             ColorHelper.DARK_RED + "off"));
        }
        onToggle(getStatus());
    }

    public int getKeybind()
    {
        return keybind;
    }

    public void setKeybind(final int i)
    {
        keybind = i;
    }

    public int getColor()
    {
        return color;
    }

    public void setColor(final int i)
    {
        color = i;
    }

    public ModuleType getType()
    {
        return type;
    }

    public void setType(final ModuleType mt)
    {
        type = mt;
    }

    public void setNotToggleable(final boolean b)
    {
        notToggleable = b;
    }

    public boolean isNotArraylistRenderable()
    {
        return notArraylistRenderable;
    }

    public void setNotArraylistRenderable(final boolean b)
    {
        notArraylistRenderable = b;
    }

    protected void println(final String text)
    {
        HeliosLogger.getInstance().logChat(String.format("%sModule", ColorHelper.DARK_AQUA), text);
    }
}
