package com.not2excel.helios.module.pvp;

import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.event.player.EventPostMotionUpdate;
import com.not2excel.helios.event.player.EventPreMotionUpdate;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.helios.module.ModuleType;
import com.not2excel.lib.event.EventHandler;
import net.minecraft.src.EnumMovingObjectType;
import net.minecraft.src.Material;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/2/13
 */
public class Criticals extends ModuleBase
{
    private boolean prevGround = true;

    public Criticals()
    {
        super("Criticals", "Not2EXceL", "Criticals derp", ModuleType.PVP);
    }

    @EventHandler(event = EventPreMotionUpdate.class)
    public void preMotionUpdate()
    {
        prevGround = Wrapper.getInstance().getPlayer().onGround;
        if (Wrapper.getInstance().getMinecraft().objectMouseOver == null)
        { return; }
        if (canCrit() && Wrapper.getInstance().getPlayer().isSwingInProgress &&
            Wrapper.getInstance().getMinecraft().objectMouseOver.typeOfHit == EnumMovingObjectType.ENTITY)
        {
            Wrapper.getInstance().getPlayer().onGround = false;
            double prevY = Wrapper.getInstance().getPlayer().posY;
            Wrapper.getInstance().getPlayer().boundingBox.offset(0, 0.08, 0);
            Wrapper.getInstance().getPlayer().posY = prevY;
        }
    }

    @EventHandler(event = EventPostMotionUpdate.class)
    public void postMotionUpdate()
    {
        Wrapper.getInstance().getPlayer().onGround = prevGround;
    }

    @Override
    public String[] getAliases()
    {
        return new String[]{"crits", "criticals"};
    }

    @Override
    public String getSyntax()
    {
        return "";
    }

    private boolean canCrit()
    {
        return Wrapper.getInstance().getPlayer().onGround &&
               !Wrapper.getInstance().getPlayer().isInWater() &&
               !Wrapper.getInstance().getPlayer().isInsideOfMaterial(Material.lava) &&
               !Wrapper.getInstance().getPlayer().isInsideOfMaterial(Material.web) &&
               Wrapper.getInstance().getPlayer().isCollidedVertically &&
               Wrapper.getInstance().getPlayer().isSwingInProgress &&
               Wrapper.getInstance().getPlayerController().curBlockDamageMP <= 0F;
    }
}
