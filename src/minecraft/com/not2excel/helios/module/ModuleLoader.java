package com.not2excel.helios.module;

import com.not2excel.helios.module.networking.ActionPVPStats;
import com.not2excel.helios.module.networking.TwitchStreamChecker;
import com.not2excel.helios.module.pvp.Criticals;
import com.not2excel.helios.module.render.*;
import com.not2excel.lib.module.ModuleManager;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class ModuleLoader
{
    private static volatile ModuleLoader instance;

    public static ModuleLoader getInstance()
    {
        if (instance == null)
        { instance = new ModuleLoader(); }
        return instance;
    }

    public void loadModules()
    {
        registerModule(new ActionPVPStats());
        registerModule(new ArrayList());
        registerModule(new BlockBreakAnimation());
        registerModule(new Criticals());
        registerModule(new Cyadd());
        registerModule(new HitSpheres());
        registerModule(new MorganFreeman());
        registerModule(new Tracers());
        registerModule(new TwitchStreamChecker());

//        Class enumerator is throwing instantiationexception and i cba to find it atm
//
//        List<Class<?>> moduleClasses = new LinkedList<Class<?>>();
//        List<Class<?>> renderClasses = ClassEnumerator.getInstance().getClassesForPackage(getClass().getPackage());
//        //create a list per module category package
//        moduleClasses.addAll(renderClasses);
//        //add all lists together by addAll for each module category package
//        for (Class<?> c : moduleClasses)
//        {
//            if (Module.class.isAssignableFrom(c))
//            {
//                try
//                { ModuleManager.getInstance().registerModule((Module) c.newInstance()); }
//                catch (InstantiationException e)
//                { e.printStackTrace(); }
//                catch (IllegalAccessException e)
//                { e.printStackTrace(); }
//            }
//        }
    }

    private void registerModule(ModuleBase moduleBase)
    {
        ModuleManager.getInstance().registerModule(moduleBase);
    }
}
