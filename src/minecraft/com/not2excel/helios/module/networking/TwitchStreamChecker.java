package com.not2excel.helios.module.networking;

import com.not2excel.helios.client.Utilities;
import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.event.player.EventPreUpdate;
import com.not2excel.helios.event.render.EventRenderGui;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.helios.module.ModuleType;
import com.not2excel.helios.util.color.ColorHelper;
import com.not2excel.lib.event.EventHandler;
import com.not2excel.lib.command.Command;
import com.not2excel.lib.networking.misc.StreamTwitch;
import com.not2excel.lib.time.TimeManager;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class TwitchStreamChecker extends ModuleBase implements Command
{
    private StreamTwitch streamTwitch;
    private TimeManager  timeManager;

    public TwitchStreamChecker()
    {
        super("Twitch Stream Checker", "Not2EXceL", "Checks if a stream is online", ModuleType.RENDER);
        setStatus(true);
        setNotArraylistRenderable(true);
        streamTwitch = new StreamTwitch("actionpvp");
        timeManager = new TimeManager();
        timeManager.resetLastTime();
    }

    @EventHandler(event = EventRenderGui.class)
    public void renderOnline()
    {
        if (streamTwitch.isOnline())
        {
            String s = String.format("Stream: %s is %sonline.",
                                     ColorHelper.GOLD +
                                     streamTwitch.getChannel() +
                                     ColorHelper.WHITE,
                                     ColorHelper.GREEN);
            Wrapper.getInstance().getFontRenderer().drawStringWithShadow(s,
                                                                         Utilities.getInstance().getScaledWidth() / 2 -
                                                                         Wrapper.getInstance().getFontRenderer()
                                                                                .getStringWidth(s) / 2, 2, 0xFFffffff);
        }
    }

    @EventHandler(event = EventPreUpdate.class)
    public void checkStream()
    {
        if (timeManager.sleepMillis(streamTwitch.isOnline() ? 600000 : 300000))
        {
            streamTwitch.refreshStream();
            if (streamTwitch.isOnline())
            {
                println(String.format("Stream: %s is %sonline.",
                                      ColorHelper.GOLD +
                                      streamTwitch.getChannel() +
                                      ColorHelper.WHITE,
                                      ColorHelper.GREEN));
            }
            timeManager.resetLastTime();
        }
    }

    @Override
    public String[] getAliases()
    {
        return new String[]{"twitch", "stream"};
    }

    @Override
    public String getSyntax()
    {
        return "-channel <x>";
    }

    @Override
    public void onCommand(final String fullCommand, final String[] args)
    {
        for (final String s : args)
        {
            if (s.startsWith("channel"))
            {
                streamTwitch.setChannel(s.substring(8));
                streamTwitch.refreshStream();
                if (streamTwitch.isOnline())
                {
                    println(String.format("Stream: %s is %sonline.",
                                          ColorHelper.GOLD +
                                          streamTwitch.getChannel() +
                                          ColorHelper.WHITE,
                                          ColorHelper.GREEN));
                }
            }
        }
    }
}