package com.not2excel.helios.module.networking;

import com.not2excel.helios.event.render.EventRenderGui;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.helios.module.ModuleType;
import com.not2excel.helios.util.color.ColorHelper;
import com.not2excel.lib.event.EventHandler;
import com.not2excel.lib.command.Command;
import com.not2excel.lib.networking.misc.ActionPVPKD;

import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/29/13
 */
public class ActionPVPStats extends ModuleBase implements Command
{
    private ActionPVPKD actionPVPKD;
    private DecimalFormat decimalFormat = new DecimalFormat("##.##");

    public ActionPVPStats()
    {
        super("ActionPVP Stats", "Not2EXceL", "Gets ActionPVP Stats", ModuleType.RENDER);
        actionPVPKD = new ActionPVPKD();
        setNotArraylistRenderable(true);
    }

    @Override
    public void onCommand(final String fullCommand, final String[] args)
    {
        for (final String s : args)
        {
            if (s.startsWith("player"))
            {
                actionPVPKD.setPlayer(s.substring(7).trim());
                actionPVPKD.getKD();
                println(String.format("%s ActionPVP Stats:", ColorHelper.GOLD + actionPVPKD.getPlayer()));
                println(String.format("%s%s%s",
                                      ColorHelper.WHITE + "Kills: " + ColorHelper.GREEN + actionPVPKD.getKdMap().get(
                                              "kills"),
                                      ColorHelper.WHITE + "Deaths: " + ColorHelper.RED + actionPVPKD.getKdMap().get(
                                              "deaths"),
                                      ColorHelper.WHITE + "Ratio: " + (Double.parseDouble(actionPVPKD.getKdMap().get(
                                              "ratio")) > 1 ? ColorHelper.GREEN : ColorHelper.RED) +
                                      decimalFormat.format(Double.parseDouble(actionPVPKD.getKdMap().get("ratio")))));
            }
            if (s.startsWith("stats"))
            {
                println(String.format("%s ActionPVP Stats:", ColorHelper.GOLD + actionPVPKD.getPlayer()));
                println(String.format("%s%s%s",
                                      ColorHelper.WHITE + "Kills: " + ColorHelper.GREEN + actionPVPKD.getKdMap().get(
                                              "kills"),
                                      ColorHelper.WHITE + "Deaths: " + ColorHelper.RED + actionPVPKD.getKdMap().get(
                                              "deaths"),
                                      ColorHelper.WHITE + "Ratio: " + (Double.parseDouble(actionPVPKD.getKdMap().get(
                                              "ratio")) > 1 ? ColorHelper.GREEN : ColorHelper.RED) +
                                      decimalFormat.format(Double.parseDouble(actionPVPKD.getKdMap().get("ratio")))));
            }
        }
    }

    @EventHandler(event = EventRenderGui.class)
    public void renderStats()
    {

    }

    @Override
    public String[] getAliases()
    {
        return new String[]{"actionpvp", "stats"};
    }

    @Override
    public String getSyntax()
    {
        return "-player <x>";
    }
}
