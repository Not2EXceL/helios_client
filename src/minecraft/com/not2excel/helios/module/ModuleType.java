package com.not2excel.helios.module;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public enum ModuleType
{
    RENDER,
    PLAYER,
    WORLD,
    PVP,
    AUTO,
    COMMAND,
    NONE;
}
