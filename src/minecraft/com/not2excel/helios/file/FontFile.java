package com.not2excel.helios.file;

import com.not2excel.helios.event.client.EventLoad;
import com.not2excel.helios.util.betterfonts.FontHandler;
import com.not2excel.lib.event.EventHandler;
import com.not2excel.lib.file.FileBase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/21/13
 */
public class FontFile extends FileBase
{
    public FontFile()
    {
        super("Font");
    }

    @EventHandler(event = EventLoad.class)
    public void load()
    {
        loadFile();
    }

    @Override
    public void loadFile()
    {
        NodeList nodeList = xmlManager.parseXML("betterfonts");
        for (int i = 0; i < nodeList.getLength(); i++)
        {
            final Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) node;
                System.out.println(element.getAttribute("global"));
                FontHandler.getInstance().setGlobalTTF(Boolean.valueOf(element.getAttribute("global")));
                FontHandler.getInstance().setFontName(element.getElementsByTagName("fontName").item(0)
                                                             .getTextContent());
                FontHandler.getInstance().setFontSize(Integer.parseInt(element.getElementsByTagName("fontSize").item(0)
                                                             .getTextContent()));
                FontHandler.getInstance().initializeFontRenderer();
            }
        }
    }

    @Override
    public void saveFile()
    {
        Document document = xmlManager.startGenerateXML();
        Element rootElement = xmlManager.generateRootElement(document);

        Element betterfonts = document.createElement("betterfonts");
        rootElement.appendChild(betterfonts);
        betterfonts.setAttribute("global", String.valueOf(FontHandler.getInstance().isGlobalTTF()));

        Element fontName = document.createElement("fontName");
        fontName.appendChild(document.createTextNode(FontHandler.getInstance().getFontName()));
        betterfonts.appendChild(fontName);

        Element fontSize = document.createElement("fontSize");
        fontSize.appendChild(document.createTextNode(String.valueOf(FontHandler.getInstance().getFontSize())));
        betterfonts.appendChild(fontSize);

        xmlManager.finishGenerateXML(document);
    }
}
