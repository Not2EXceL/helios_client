package com.not2excel.helios.keybind;

import com.not2excel.helios.client.Utilities;
import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.event.client.EventKeyPressed;
import com.not2excel.helios.logger.HeliosLogger;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.lib.event.EventHandler;
import com.not2excel.lib.event.EventManager;
import com.not2excel.lib.event.FlexibleEventListener;
import com.not2excel.lib.module.Module;
import com.not2excel.lib.module.ModuleManager;
import org.lwjgl.input.Keyboard;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/20/13
 */
public class KeybindManager implements FlexibleEventListener
{
    private static volatile KeybindManager instance;
    private final Map<Integer, String> keyMap = new HashMap<Integer, String>();

    public KeybindManager()
    {
        EventManager.getInstance().registerListener(this);
    }

    public static KeybindManager getInstance()
    {
        if (instance == null)
        { instance = new KeybindManager(); }
        return instance;
    }

    public void loadKeybinds()
    {
        for (final Module module : ModuleManager.getInstance().getModuleArray())
        {
            if (!(module instanceof ModuleBase))
            { continue; }
            final ModuleBase moduleBase = (ModuleBase) module;
            if (!moduleBase.isToggleable())
            { continue; }
            final int key = moduleBase.getKeybind();
            final String command = ".toggle" + moduleBase.getAliases()[0];
            synchronized (keyMap)
            { keyMap.put(key, command); }
        }
    }

    @EventHandler(event = EventKeyPressed.class)
    public void checkKeyMap(EventKeyPressed event)
    {
        final int key = event.getKey();
        if (!keyMap.containsKey(key))
        { return; }
        final String command = keyMap.get(key);
        Utilities.getInstance().sendMessage(command);
    }

    public void registerKeybind(final int key, final String command)
    {
        if (!keyMap.containsKey(key))
        {
            synchronized (keyMap)
            { keyMap.put(key, command); }
            if (Keyboard.getKeyName(key) == null)
            { return; }
            if (Wrapper.getInstance().getPlayer() != null)
            { HeliosLogger.getInstance().logChat("Keybind", "Key '" + Keyboard.getKeyName(key) +
                                                            "' bound to: " + command); }
            else
            { HeliosLogger.getInstance().logConsole("Keybind", "Key '" + Keyboard.getKeyName(key) + "' bound to: " + command); }
        }
        else
        {
            synchronized (keyMap)
            {
                keyMap.remove(key);
                keyMap.put(key, command);
            }
            if (Keyboard.getKeyName(key) == null)
            { return; }
            if (Wrapper.getInstance().getPlayer() != null)
            { HeliosLogger.getInstance().logChat("Keybind", "Key '" + Keyboard.getKeyName(key) +
                                                            "' modified to: " + command); }
            else
            { HeliosLogger.getInstance().logConsole("Keybind", "Key '" + Keyboard.getKeyName(key) + "' modified to: " + command); }
        }
    }
}
