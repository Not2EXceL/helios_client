package com.not2excel.helios.command;

import com.not2excel.helios.client.Variables;
import com.not2excel.helios.logger.HeliosLogger;
import com.not2excel.helios.util.color.ColorHelper;
import com.not2excel.lib.command.CommandBase;
import com.not2excel.lib.command.CommandManager;
import com.not2excel.helios.module.ModuleBase;
import com.not2excel.helios.module.ModuleType;
import com.not2excel.lib.command.Command;
import com.not2excel.lib.module.Module;
import com.not2excel.lib.module.ModuleManager;
import net.minecraft.src.MathHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/26/13
 */
public class CommandLoader
{
    private static volatile CommandLoader instance;

    public static CommandLoader getInstance()
    {
        if (instance == null)
        { instance = new CommandLoader(); }
        return instance;
    }

    public void loadCommands()
    {
        registerCommand(new CommandBase("Help", "Displays chat based help gui", ".help <page #>")
        {
            final List<String> commands = new ArrayList<String>();

            @Override
            public void onCommand(String fullCommand, String[] args)
            {
                populateCommands();
                if (fullCommand.equalsIgnoreCase(getCommand()))
                { displayPage(0); }
                else
                { displayPage(Integer.parseInt(fullCommand.substring(5))); }
            }

            void displayPage(int page)
            {
                if (page == 0)
                {
                    println(String.format("%sWelcome to %s help.",
                                          ColorHelper.GOLD, Variables.getInstance().CLIENT_TITLE));
                    println("Use .aliases <command> to see aliases for a command.");
                    println("Use .toggle <module> to toggle a module.");
                    println("Use arg -toggle or -alias to toggle or see aliases");
                    println("Use .help <page> to display more commands.");
                    println(String.format("%s%s commands loaded.", ColorHelper.DARK_AQUA, commands.size()));
                }
                else
                {
                    int cMin = page * 5 - 5;
                    int cMax = page * 5;
                    if (cMin > commands.size() - 1)
                    {
                        println(String.format("%sHelp page %s%s%s does not exist.", ColorHelper.DARK_RED,
                                              ColorHelper.GOLD, page, ColorHelper.DARK_RED));
                        return;
                    }
                    println(String.format("%sHelp page: %s%s/%s", ColorHelper.GOLD, ColorHelper.WHITE, page,
                                          MathHelper.floor_double(commands.size() / 5)));
                    println(ColorHelper.DARK_AQUA + "------------------------------------------------------");
                    for (int i = cMin; i < cMax; i++)
                    {
                        if (i > commands.size() - 1)
                        { break; }
                        println(commands.get(i));
                    }
                    println(ColorHelper.DARK_AQUA + "------------------------------------------------------");
                    println(String.format("%sUse .help %s for page %s", ColorHelper.GOLD, page + 1, page + 1));
                }
            }

            void populateCommands()
            {
                commands.clear();
                for (final CommandBase c : CommandManager.getInstance().getCommandList())
                { commands.add(String.format("%s: %s", c.getCommand(), c.getSyntax())); }
                for (final Module m : ModuleManager.getInstance().getModuleArray())
                {
                    if (m instanceof Command)
                    {
                        commands.add(String.format("%s: %s %s", m.getLabel(), ((ModuleBase) m).getAliases()[0],
                                                   ((ModuleBase) m).getSyntax()));
                    }
                    else
                    {
                        commands.add(String.format("%s: %s %s", m.getLabel(), ".toggle",
                                                   ((ModuleBase) m).getAliases()[0]));
                    }
                }
            }

            private void println(String text)
            {
                HeliosLogger.getInstance().logChat(ColorHelper.DARK_AQUA + "Help" + ColorHelper.WHITE, text);
            }
        });
        registerCommand(new CommandBase("Toggle", "Toggles a module", ".toggle <mod>")
        {
            @Override
            public void onCommand(String fullCommand, String[] args)
            {
                if (fullCommand.equalsIgnoreCase(getCommand()))
                { println(String.format("%s: %s", getCommand(), getSyntax())); }
                else
                {
                    String[] nameArgs = fullCommand.toLowerCase().split(" ");
                    for (Module m : ModuleManager.getInstance().getModuleArray())
                    {
                        for (String alias : Arrays.asList(((ModuleBase) m).getAliases()))
                        {
                            if (alias.equalsIgnoreCase(nameArgs[1]))
                            { ((ModuleBase) m).toggleModule(true); }
                        }
                    }
                    for (String s : args)
                    {
                        String moduleName = "";
                        if (s.toLowerCase().startsWith("mod"))
                        { moduleName = s.substring(4); }
                        else if (s.toLowerCase().startsWith("module"))
                        { moduleName = s.substring(7); }
                        for (Module m : ModuleManager.getInstance().getModuleArray())
                        {
                            for (String alias : Arrays.asList(((ModuleBase) m).getAliases()))
                            {
                                if (alias.equalsIgnoreCase(moduleName))
                                { ((ModuleBase) m).toggleModule(true); }
                            }
                        }
                    }
                }
            }

            private void println(String text)
            {
                HeliosLogger.getInstance().logChat(ColorHelper.DARK_AQUA + "Toggle" + ColorHelper.WHITE, text);
            }
        });
        registerCommand(new CommandBase("Aliases", "Displays aliases for command or module", ".aliases <cmd/mod>")
        {
            @Override
            public void onCommand(String fullCommand, String[] args)
            {
                if (fullCommand.equalsIgnoreCase(getCommand()))
                { println(String.format("%s: %s", getCommand(), getSyntax())); }
                else
                {
                    for (String s : args)
                    {
                        String aliasName = "";
                        if (s.toLowerCase().startsWith("aliases"))
                        { aliasName = s.substring(8); }
                        for (CommandBase c : CommandManager.getInstance().getCommandList())
                        {
                            if (c.getCommand().equalsIgnoreCase(aliasName))
                            {
                                println(String.format("%s%s:%s %s", ColorHelper.GOLD, c.getCommand(), ColorHelper.WHITE,
                                                      c.getDescription()));
                                println(String.format("%s%s", ColorHelper.GREY, c.getSyntax()));
                            }
                        }
                        for (Module m : ModuleManager.getInstance().getModuleArray())
                        {
                            for (String alias : Arrays.asList(((ModuleBase) m).getAliases()))
                            {
                                if (alias.equalsIgnoreCase(aliasName))
                                {
                                    println(String.format("%s%s:%s %s", ColorHelper.GOLD, m.getLabel(), ColorHelper.WHITE,
                                                          m.getDescription()));
                                    println(String.format("%s%s %s", ColorHelper.GREY, ((ModuleBase) m).getAliases()[0],
                                                          ((ModuleBase) m).getSyntax()));
                                }
                            }
                        }
                    }
                }
            }

            private void println(String text)
            {
                HeliosLogger.getInstance().logChat(ColorHelper.DARK_AQUA + "Aliases" + ColorHelper.WHITE, text);
            }
        });

        registerCommand(new CommandBase("Legit", "Displays Legit version of mc", ".legit")
        {
            @Override
            public void onCommand(String fullCommand, String[] args)
            {
                HeliosLogger.getInstance().logChat("LEL", "Gameguides STFU");
            }
        });
    }

    private void registerCommand(CommandBase commandBase)
    {
        CommandManager.getInstance().registerCommand(commandBase);
    }

    private void unregisterCommand(CommandBase commandBase)
    {
        CommandManager.getInstance().unregisterCommand(commandBase);
    }


    public void runCommands(final String input)
    {
        final boolean chat = input.startsWith(Variables.getInstance().CHAT_PREFIX);
        final String fullCommand = chat ? input.substring(1) : input;
        final String[] args = fullCommand.split(Variables.getInstance().COMMAND_SPLIT);
        for (int i = 0; i < args.length; i++)
        {
            String s = args[i];
            args[i] = s.trim();
        }

        for (final CommandBase commandBase : CommandManager.getInstance().getCommandList())
        {
            if (commandBase.getCommand().equalsIgnoreCase(args[0]) ||
                fullCommand.toLowerCase().startsWith(commandBase.getCommand().toLowerCase()))
            {
                commandBase.onCommand(fullCommand, args);
                return;
            }
        }

        for (final Module module : ModuleManager.getInstance().getModuleArray())
        {
            if (module instanceof Command)
            {
                final Command c = (Command) module;
                final List strings = Arrays.asList(((ModuleBase) module).getAliases());
                if (strings.contains(args[0].toLowerCase().trim()))
                {
                    if (args.length == 1)
                    {
                        if (((ModuleBase) module).getType() == ModuleType.COMMAND ||
                            ((ModuleBase) module).getType() == ModuleType.NONE)
                        { ((Command) module).onCommand(fullCommand, args); }
                        else
                        {
                            HeliosLogger.getInstance().logChat("Module", String.format("%s%s Aliases:",
                                                                                       ColorHelper.DARK_AQUA,
                                                                                       module.getLabel()));
                            for (String alias : ((ModuleBase) module).getAliases())
                            {
                                HeliosLogger.getInstance().logChat("Aliases",
                                                                   String.format("%s %s", alias,
                                                                                 ((ModuleBase) module)
                                                                                         .getSyntax()));
                            }
                        }
                    }
                    else
                    {
                        for (final String s : args)
                        {
                            if (s.startsWith("toggle"))
                            { ((ModuleBase) module).toggleModule(true); }
                            if (s.startsWith("aliases"))
                            {
                                HeliosLogger.getInstance().logChat("Module", String.format("%s%s Aliases:",
                                                                                           ColorHelper.DARK_AQUA,
                                                                                           module.getLabel()));
                                for (String alias : ((ModuleBase) module).getAliases())
                                {
                                    HeliosLogger.getInstance().logChat("Aliases",
                                                                       String.format("%s %s", alias,
                                                                                     ((ModuleBase) module)
                                                                                             .getSyntax()));
                                }
                            }
                        }
                        c.onCommand(fullCommand, args);
                    }
                    return;
                }
            }
            else
            {

            }
        }
        HeliosLogger.getInstance().logChat("Error", String.format("%sCould not find command using %s%s",
                                                                  ColorHelper.DARK_RED, ColorHelper.GOLD, input));
    }
}
