package com.not2excel.helios.client;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class Variables
{
    private static volatile Variables instance;
    public final String  CLIENT_TITLE   = "Helios";
    public final String  CLIENT_VERSION = "0.9";
    public final String  CHAT_PREFIX    = ".";
    public final String  COMMAND_SPLIT  = "-";
    private      boolean noCheatMode    = false;

    public static Variables getInstance()
    {
        if (instance == null)
        { instance = new Variables(); }
        return instance;
    }

    public boolean isNoCheatMode()
    {
        return noCheatMode;
    }

    public void setNoCheatMode(final boolean b)
    {
        noCheatMode = b;
    }
}
