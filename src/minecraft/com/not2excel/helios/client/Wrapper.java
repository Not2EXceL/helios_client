package com.not2excel.helios.client;

import net.minecraft.src.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class Wrapper
{
    private static volatile Wrapper instance;

    public static Wrapper getInstance()
    {
        if (instance == null)
        { instance = new Wrapper(); }
        return instance;
    }

    public Minecraft getMinecraft()
    {
        return Minecraft.getMinecraft();
    }

    public EntityClientPlayerMP getPlayer()
    {
        return getMinecraft().thePlayer;
    }

    public FontRenderer getFontRenderer()
    {
        return getMinecraft().fontRenderer;
    }

    public GameSettings getGameSettings()
    {
        return getMinecraft().gameSettings;
    }

    public TextureManager getRenderEngine()
    {
        return getMinecraft().renderEngine;
    }

    public GuiIngame getGuiIngame()
    {
        return getMinecraft().ingameGUI;
    }

    public WorldClient getWorld()
    {
        return getMinecraft().theWorld;
    }

    public NetClientHandler getNetHandler()
    {
        return getMinecraft().getNetHandler();
    }

    public PlayerControllerMP getPlayerController()
    {
        return getMinecraft().playerController;
    }
}
