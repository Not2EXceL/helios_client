package com.not2excel.helios.client;

import com.not2excel.helios.util.color.ColorHelper;
import net.minecraft.src.Minecraft;
import net.minecraft.src.Packet;
import net.minecraft.src.ScaledResolution;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.security.CodeSource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class Utilities
{
    private static volatile Utilities instance;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
    private final Calendar         calendar         = Calendar.getInstance();

    public static Utilities getInstance()
    {
        if (instance == null)
        { instance = new Utilities(); }
        return instance;
    }

    public File getClientDir()
    {
        final File clientDir = new File(Wrapper.getInstance().getMinecraft().mcDataDir,
                                        Variables.getInstance().CLIENT_TITLE);
        if (!clientDir.exists())
        { clientDir.mkdirs(); }
        return clientDir;
    }

    public void sendPacket(Packet packet)
    {
        Wrapper.getInstance().getNetHandler().addToSendQueue(packet);
    }

    public void sendMessage(String message)
    {
        Wrapper.getInstance().getPlayer().sendChatMessage(message);
    }

    public void addMessage(final String message)
    {
        Wrapper.getInstance().getPlayer().addChatMessage(message);
    }

    private String getCurrentTime()
    {
        return simpleDateFormat.format(calendar.getTime());
    }

    public ScaledResolution getScaledResolution()
    {
        return new ScaledResolution(Wrapper.getInstance().getGameSettings(),
                                    Wrapper.getInstance().getMinecraft().displayWidth,
                                    Wrapper.getInstance().getMinecraft().displayHeight);
    }

    public int getScaledWidth()
    {
        return getScaledResolution().getScaledWidth();
    }

    public int getScaledHeight()
    {
        return getScaledResolution().getScaledHeight();
    }

    public String clientUpdateString()
    {
        int i = isClientUpdated();
        if (i == 0)
        { return ColorHelper.DARK_GREEN + "Client up to date."; }
        if (i == 1)
        { return ColorHelper.GOLD + "Update available."; }
        if (i == 2)
        { return ColorHelper.DARK_RED + "Failed to get client version."; }
        return "";
    }

    public int isClientUpdated()
    {
        if (isInDebugMode())
        { return 0; }
        String version = getClientWebVersion();
        int returnValue = -1;
        if (version.equalsIgnoreCase(Variables.getInstance().CLIENT_VERSION))
        { returnValue = 0; }
        else if (version.equalsIgnoreCase("Failed to get client version."))
        { returnValue = 2; }
        else if (!version.equalsIgnoreCase(Variables.getInstance().CLIENT_VERSION))
        { returnValue = 1; }
        return returnValue;
    }

    public String getClientWebVersion()
    {
        String urlString = "http://mc.not2excel.com/HeliosVersion";
        try
        {
            List<String> list = new ArrayList<String>();
            URL url = new URL(urlString);
            URLConnection urlConnection = url.openConnection();
            InputStreamReader inputStream = new InputStreamReader(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(inputStream);
            for (String s; (s = reader.readLine()) != null; )
            { list.add(s); }
            inputStream.close();
            reader.close();
            for (String s : list)
            {
                String[] sa = s.split(":");
                if (sa[0].equalsIgnoreCase("Client Version"))
                { return sa[1]; }
            }
        }
        catch (Exception e)
        { e.printStackTrace(); }
        return "Failed to get client version.";
    }

    public boolean isInDebugMode()
    {
        if (getPath().endsWith(".jar"))
        { return false; }
        return true;
    }

    private String getPath()
    {
        CodeSource codeSource = Minecraft.class.getProtectionDomain().getCodeSource();
        String s = null;
        try
        {
            File sourceFile = new File(codeSource.getLocation().toURI().getPath());
            s = "" + sourceFile;
        }
        catch (URISyntaxException err)
        { err.printStackTrace(); }
        return s;
    }

    public List<String> listFormattedStringToWidth(String s, int width)
    {
        return Arrays.asList(wrapFormattedStringToWidth(s, width).split("\n"));
    }

    String wrapFormattedStringToWidth(String s, float width)
    {
        int wrapWidth = sizeStringToWidth(s, width);
        if (s.length() <= wrapWidth)
        { return s; }
        String split = s.substring(0, wrapWidth);
        String split2 = getFormatFromString(split) + s.substring(wrapWidth + (s.charAt(wrapWidth) == ' ' ? 1 : 0));
        return split + "\n" + wrapFormattedStringToWidth(split2, width);
    }

    private int sizeStringToWidth(String par1Str, float par2)
    {
        int var3 = par1Str.length();
        float var4 = 0.0F;
        int var5 = 0;
        int var6 = -1;
        for (boolean var7 = false; var5 < var3; var5++)
        {
            char var8 = par1Str.charAt(var5);
            switch (var8)
            {
                case '\n':
                    var5--;
                    break;
                case '§':
                    if (var5 < var3 - 1)
                    {
                        var5++;
                        char var9 = par1Str.charAt(var5);
                        if ((var9 != 'l') && (var9 != 'L'))
                        {
                            if ((var9 == 'r') || (var9 == 'R') || (isFormatColor(var9)))
                            { var7 = false; }
                        }
                        else
                        { var7 = true; }
                    }
                    break;
                case ' ':
                    var6 = var5;
                case '-':
                    var6 = var5;
                case '_':
                    var6 = var5;
                case ':':
                    var6 = var5;
                default:
                    String text = String.valueOf(var8);
                    var4 += Wrapper.getInstance().getFontRenderer().getStringWidth(text);
                    if (var7)
                    { var4 += 1.0F; }
                    break;
            }
            if (var8 == '\n')
            {
                var5++;
                var6 = var5;
            }
            else
            {
                if (var4 > par2)
                { break; }
            }
        }
        return (var5 != var3) && (var6 != -1) && (var6 < var5) ? var6 : var5;
    }

    private String getFormatFromString(String par0Str)
    {
        String var1 = "";
        int var2 = -1;
        int var3 = par0Str.length();
        while ((var2 = par0Str.indexOf('§', var2 + 1)) != -1)
        {
            if (var2 < var3 - 1)
            {
                char var4 = par0Str.charAt(var2 + 1);

                if (isFormatColor(var4))
                { var1 = "§" + var4; }
                else if (isFormatSpecial(var4))
                { var1 = var1 + "§" + var4; }
            }
        }
        return var1;
    }

    private boolean isFormatColor(char par0)
    {
        return ((par0 >= '0') && (par0 <= '9')) || ((par0 >= 'a') && (par0 <= 'f')) || ((par0 >= 'A') && (par0 <= 'F'));
    }

    private boolean isFormatSpecial(char par0)
    {
        return ((par0 >= 'k') && (par0 <= 'o')) || ((par0 >= 'K') && (par0 <= 'O')) || (par0 == 'r') || (par0 == 'R');
    }
}
