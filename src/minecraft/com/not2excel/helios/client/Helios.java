package com.not2excel.helios.client;

import com.not2excel.helios.command.CommandLoader;
import com.not2excel.helios.keybind.KeybindManager;
import com.not2excel.helios.module.ModuleLoader;
import com.not2excel.lib.file.FileHandler;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/23/13
 */
public class Helios
{
    /**
     * Classes modified:
     * @Minecraft.java - main hook stuff obv
     * @ThreadConnectToServer - OverrideNetClientHandler instance creation
     * @ThreadOnlineConnect - OverrideNetClientHandler instance creation
     * @GuiOptions - added Client Options button
     * @PlayerControllerMP - changed field/method access
     * @NetClientHandler - changed field/method access
     * @GuiIngame - changed field/method access
     * @GuiSlot - changed field/method access
     * @GuiNewChat - changed field/method access, dragging && mouseMovedOrUp && mouseClicked
     * @GuiChat - mouseMovedOrUp && mouseClicked && drawScreen added and modified
     * @AxisAlignedBB - changed field/method access
     * @EntityRenderer - changed field/method access
     * @GuiScreen - changed the default background to the picture :P
     */
    private static volatile Helios instance;

    public void initClient()
    {
        ModuleLoader.getInstance().loadModules();
        CommandLoader.getInstance().loadCommands();
        KeybindManager.getInstance().loadKeybinds();
        FileHandler.getInstance().loadFiles();
    }

    public static Helios getInstance()
    {
        if (instance == null)
        { instance = new Helios(); }
        return instance;
    }
}
