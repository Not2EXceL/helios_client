package com.not2excel.helios.util.drawing;

import com.not2excel.helios.client.Utilities;
import com.not2excel.helios.client.Wrapper;
import net.minecraft.src.ResourceLocation;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created with IntelliJ IDEA.
 * User: steelers
 * Date: 6/21/13
 * Time: 10:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class Drawing2D
{
    protected static float zLevel;

    public static void drawLine(double x, double y, double x1, double y1, float width)
    {
        glLineWidth(width);
        glBegin(GL_LINES);
        glVertex2d(x, y);
        glVertex2d(x1, y1);
        glEnd();
    }

    public static void drawHLine(float par1, float par2, float par3, int par4)
    {
        if (par2 < par1)
        {
            float var5 = par1;
            par1 = par2;
            par2 = var5;
        }

        drawRect(par1, par3, par2 + 1, par3 + 1, par4);
    }

    public static void drawVLine(float par1, float par2, float par3, int par4)
    {
        if (par3 < par2)
        {
            float var5 = par2;
            par2 = par3;
            par3 = var5;
        }

        drawRect(par1, par2 + 1, par1 + 1, par3, par4);
    }

    public static void drawRect(float paramXStart, float paramYStart, float paramXEnd, float paramYEnd, int paramColor)
    {
        float alpha = (float) (paramColor >> 24 & 0xFF) / 255F;
        float red = (float) (paramColor >> 16 & 0xFF) / 255F;
        float green = (float) (paramColor >> 8 & 0xFF) / 255F;
        float blue = (float) (paramColor & 0xFF) / 255F;

        GL11Assist.defaultsOn();

        glPushMatrix();
        glColor4f(red, green, blue, alpha);
        glBegin(GL_QUADS);
        glVertex2d(paramXEnd, paramYStart);
        glVertex2d(paramXStart, paramYStart);
        glVertex2d(paramXStart, paramYEnd);
        glVertex2d(paramXEnd, paramYEnd);
        glEnd();
        glPopMatrix();

        GL11Assist.defaultsOff();
    }

    public static void drawRect(float paramXStart, float paramYStart, float paramXEnd, float paramYEnd, float red,
                                float green, float blue, float alpha)
    {
        GL11Assist.defaultsOn();

        glPushMatrix();
        glColor4f(red, green, blue, alpha);
        glBegin(GL_QUADS);
        glVertex2d(paramXEnd, paramYStart);
        glVertex2d(paramXStart, paramYStart);
        glVertex2d(paramXStart, paramYEnd);
        glVertex2d(paramXEnd, paramYEnd);
        glEnd();
        glPopMatrix();

        GL11Assist.defaultsOff();
    }

    public static void drawBorderedRect(float x, float y, float x1, float y1, int borderC, float iRed, float iGreen,
                                        float iBlue, float iAlpha)
    {
        x *= 2;
        x1 *= 2;
        y *= 2;
        y1 *= 2;
        glScalef(0.5F, 0.5F, 0.5F);
        drawVLine(x, y, y1, borderC);
        drawVLine(x1 - 1, y, y1, borderC);
        drawHLine(x, x1 - 1, y, borderC);
        drawHLine(x, x1 - 1, y1, borderC);
        drawRect(x + 1, y + 1, x1 - 1, y1, iRed, iGreen, iBlue, iAlpha);
        glScalef(2.0F, 2.0F, 2.0F);
    }

    public static void drawBorderedRect(float x, float y, float x1, float y1, int borderC, int insideC)
    {
        x *= 2;
        x1 *= 2;
        y *= 2;
        y1 *= 2;
        glScalef(0.5F, 0.5F, 0.5F);
        drawVLine(x, y, y1, borderC);
        drawVLine(x1 - 1, y, y1, borderC);
        drawHLine(x, x1 - 1, y, borderC);
        drawHLine(x, x1 - 1, y1, borderC);
        drawRect(x + 1, y + 1, x1 - 1, y1, insideC);
        glScalef(2.0F, 2.0F, 2.0F);
    }

    public static void drawBorderedRect(double x, double y, double x2, double y2, float l1, int col1, int col2)
    {
        drawRect((float) x, (float) y, (float) x2, (float) y2, col2);

        float f = (float) (col1 >> 24 & 0xFF) / 255F;
        float f1 = (float) (col1 >> 16 & 0xFF) / 255F;
        float f2 = (float) (col1 >> 8 & 0xFF) / 255F;
        float f3 = (float) (col1 & 0xFF) / 255F;
        GL11Assist.defaultsOn();

        glColor4f(f1, f2, f3, f);
        glLineWidth(l1);
        glBegin(GL_LINES);
        glVertex2d(x, y);
        glVertex2d(x, y2);
        glVertex2d(x2, y2);
        glVertex2d(x2, y);
        glVertex2d(x, y);
        glVertex2d(x2, y);
        glVertex2d(x, y2);
        glVertex2d(x2, y2);
        glEnd();

        GL11Assist.defaultsOff();
    }

    public static void drawRoundedRect(float x, float y, float x1, float y1, int borderC, int insideC)
    {
        x *= 2;
        y *= 2;
        x1 *= 2;
        y1 *= 2;
        glScalef(0.5F, 0.5F, 0.5F);
        drawVLine(x, y + 1, y1 - 2, borderC);
        drawVLine(x1 - 1, y + 1, y1 - 2, borderC);
        drawHLine(x + 2, x1 - 3, y, borderC);
        drawHLine(x + 2, x1 - 3, y1 - 1, borderC);
        drawHLine(x + 1, x + 1, y + 1, borderC);
        drawHLine(x1 - 2, x1 - 2, y + 1, borderC);
        drawHLine(x1 - 2, x1 - 2, y1 - 2, borderC);
        drawHLine(x + 1, x + 1, y1 - 2, borderC);
        drawRect(x + 1, y + 1, x1 - 1, y1 - 1, insideC);
        glScalef(2.0F, 2.0F, 2.0F);
    }

    public static void drawGradientRect(double x, double y, double x2, double y2, int col1, int col2)
    {
        float f = (float) (col1 >> 24 & 0xFF) / 255F;
        float f1 = (float) (col1 >> 16 & 0xFF) / 255F;
        float f2 = (float) (col1 >> 8 & 0xFF) / 255F;
        float f3 = (float) (col1 & 0xFF) / 255F;

        float f4 = (float) (col2 >> 24 & 0xFF) / 255F;
        float f5 = (float) (col2 >> 16 & 0xFF) / 255F;
        float f6 = (float) (col2 >> 8 & 0xFF) / 255F;
        float f7 = (float) (col2 & 0xFF) / 255F;

        GL11Assist.defaultsOn();

        glPushMatrix();
        glBegin(GL_QUADS);
        glColor4f(f1, f2, f3, f);
        glVertex2d(x2, y);
        glVertex2d(x, y);
        glColor4f(f5, f6, f7, f4);
        glVertex2d(x, y2);
        glVertex2d(x2, y2);
        glEnd();
        glPopMatrix();

        GL11Assist.defaultsOff();
    }

    public static void drawGradientBorderedRect(double x, double y, double x2, double y2, float l1, int col1, int col2,
                                                int col3)
    {
        GL11Assist.defaultsOn();
        drawGradientRect(x, y, x2, y2, col2, col3);
        x *= 2;
        y *= 2;
        x2 *= 2;
        y2 *= 2;
        GL11.glScaled(0.5, 0.5, 0.5);
        drawVLine((float) x, (float) y, (float) y2, col1);
        drawVLine((float) x2 - 1, (float) y, (float) y2, col1);
        drawHLine((float) x, (float) x2 - 1, (float) y, col1);
        drawHLine((float) x, (float) x2 - 1, (float) y2, col1);
        GL11.glScaled(2, 2, 2);
        GL11Assist.defaultsOff();
    }

    public static void drawImage(float x, float y, int imageWidth, int imageHeight, String image)
    {
        GL11Assist.defaultsOnImage();
        glColor4f(1, 1, 1, 1);
        ResourceLocation imageLoc = new ResourceLocation(image);
        Wrapper.getInstance().getRenderEngine().func_110577_a(imageLoc);
        renderTexture(x, y, imageWidth, imageHeight);
        GL11Assist.defaultsOffImage();
    }

    public static void drawHeliosBackground()
    {
        GL11.glPushMatrix();
        drawImage(0, 0, Utilities.getInstance().getScaledWidth(),
                  Utilities.getInstance().getScaledHeight(), "helios/helios_background.png");
        GL11.glPopMatrix();

    }

    public static void renderTexture(double x, double y, double width, double height)
    {
        GL11.glScaled(0.5, 0.5, 0.5);
        x *= 2;
        y *= 2;
        width *= 2;
        height *= 2;
        GL11.glBegin(4);
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex2d(x + width, y);
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex2d(x, y);
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex2d(x, y + height);
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex2d(x, y + height);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex2d(x + width, y + height);
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex2d(x + width, y);
        GL11.glEnd();
    }
}
