package com.not2excel.helios.util.drawing;

import com.not2excel.helios.client.Utilities;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_MULTISAMPLE;
import static org.lwjgl.opengl.GL13.GL_SAMPLE_ALPHA_TO_COVERAGE;


/**
 * Created with IntelliJ IDEA.
 * User: steelers
 * Date: 6/20/13
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class GL11Assist
{
    /**
     * Enable: Blend, Line Smooth
     * Disable: Texture 2D, Lighting
     * Blend Alpha, One minus Alpha
     */
    public static void defaultsOn3D(final boolean disableDepth)
    {
        glPushMatrix();
        glEnable(GL_BLEND);
        if (!disableDepth)
        { glDisable(GL_DEPTH_TEST); }
        glDisable(GL_LIGHTING);
        glEnable(GL_LINE_SMOOTH);
        glDisable(GL_TEXTURE_2D);
        glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_MULTISAMPLE);
        glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
        glShadeModel(GL_SMOOTH);
    }

    /**
     * Disable: Blend, Line Smooth
     * Enable: Texture 2D, Lighting
     */
    public static void defaultsOff3D()
    {
        glDisable(GL_BLEND);
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_LINE_SMOOTH);
        glDisable(GL_MULTISAMPLE);
        glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
        glEnable(GL_DEPTH_TEST);
        glPopMatrix();
    }

    public static void defaultsOnGui()
    {
        glPushMatrix();
        glEnable(GL_LINE_SMOOTH);
        glDisable(GL_TEXTURE_2D);
        glShadeModel(GL_SMOOTH);
        glDisable(GL_ALPHA_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    public static void defaultsOffGui()
    {
        glDisable(GL_BLEND);
        glEnable(GL_ALPHA_TEST);
        glShadeModel(GL_FLAT);
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_LINE_SMOOTH);
        glPopMatrix();
    }

    public static void defaultsOnImage()
    {
        glPushMatrix();
        glEnable(GL_LINE_SMOOTH);
        glShadeModel(GL_SMOOTH);
        glDisable(GL_ALPHA_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    public static void defaultsOffImage()
    {
        glDisable(GL_BLEND);
        glEnable(GL_ALPHA_TEST);
        glShadeModel(GL_FLAT);
        glDisable(GL_LINE_SMOOTH);
        glPopMatrix();
    }

    public static void defaultsOn()
    {
        glPushMatrix();
        glEnable(GL_LINE_SMOOTH);
        glDisable(GL_TEXTURE_2D);
        glShadeModel(GL_SMOOTH);
    }

    public static void defaultsOff()
    {
        glShadeModel(GL_FLAT);
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_LINE_SMOOTH);
        glPopMatrix();
    }

    public static float[] getRGBA(int RGBA_HEX)
    {
        float r = (float) (RGBA_HEX >> 16 & 255) / 255F;
        float g = (float) (RGBA_HEX >> 8 & 255) / 255F;
        float b = (float) (RGBA_HEX & 255) / 255F;
        float a = (float) (RGBA_HEX >> 24 & 255) / 255F;
        return new float[]{r, g, b, a};
    }

    /* created by jonalu */
    public static void prepareScissorBox(float x, float y, float x2, float y2)
    {
        int factor = Utilities.getInstance().getScaledResolution().getScaleFactor();
        glScissor((int) (x * factor), (int) ((Utilities.getInstance().getScaledHeight() - y2) * factor),
                  (int) ((x2 - x) * factor),
                  (int) ((y2 - y) * factor));
    }

    public static void enableScissor()
    {
        GL11.glPushAttrib(GL11.GL_SCISSOR_BIT);
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
    }

    public static void disableScissor()
    {
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
        GL11.glPopAttrib();
    }
}
