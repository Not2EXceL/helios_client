package com.not2excel.helios.util.betterfonts;

import com.not2excel.helios.client.Wrapper;
import com.not2excel.lib.file.FileHandler;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.Minecraft;
import net.minecraft.src.ResourceLocation;

public class FontHandler
{
    private static volatile FontHandler instance;
    private        boolean     globalTTF = true;
    private        String      fontName  = "Verdana Bold";
    private        int         fontSize  = 17;

    public static FontHandler getInstance()
    {
        if (instance == null)
        { instance = new FontHandler(); }
        return instance;
    }

    public String getFontName()
    {
        return fontName;
    }

    public void setFontName(String fontName)
    {
        this.fontName = fontName;
    }

    public int getFontSize()
    {
        return fontSize;
    }

    public void setFontSize(int fontSize)
    {
        this.fontSize = fontSize;
    }

    public boolean isGlobalTTF()
    {
        return globalTTF;
    }

    public void setGlobalTTF(boolean globalTTF)
    {
        this.globalTTF = globalTTF;
    }

    public void resetGlobalTTF(boolean status)
    {
        globalTTF = status;
        initializeFontRenderer();
    }

    public void initializeFontRenderer()
    {
        Minecraft mc = Wrapper.getInstance().getMinecraft();
        if (!isGlobalTTF())
        {
            mc.fontRenderer = mc.origFontRenderer;
            mc.standardGalacticFontRenderer = mc.origStandardGalacticFontRenderer;
        }
        else
        {
            mc.fontRenderer = new FontRenderer(mc.gameSettings, new ResourceLocation("textures/font/ascii.png"),
                                                 mc.renderEngine, false);
            mc.standardGalacticFontRenderer = new FontRenderer(mc.gameSettings, new ResourceLocation(
                    "textures/font/ascii.png"), mc.renderEngine, false);
        }
        FileHandler.getInstance().getFile("font").saveFile();
    }
}
