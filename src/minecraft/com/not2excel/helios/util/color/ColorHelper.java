package com.not2excel.helios.util.color;

import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/25/13
 */
public class ColorHelper
{
    public static final String BLACK         = "\2470";
    public static final String DARK_BLUE     = "\2471";
    public static final String DARK_GREEN    = "\2472";
    public static final String DARK_AQUA     = "\2473";
    public static final String DARK_RED      = "\2474";
    public static final String PURPLE        = "\2475";
    public static final String GOLD          = "\2476";
    public static final String GREY          = "\2477";
    public static final String DARK_GREY     = "\2478";
    public static final String BLUE          = "\2479";
    public static final String GREEN         = "\247a";
    public static final String AQUA          = "\247b";
    public static final String RED           = "\247c";
    public static final String LIGHT_PURPLE  = "\247d";
    public static final String YELLOW        = "\247e";
    public static final String WHITE         = "\247f";
    public static final String OBFUSCATED    = "\247k";
    public static final String BOLD          = "\247l";
    public static final String STRIKETHROUGH = "\247m";
    public static final String UNDERLINE     = "\247n";
    public static final String ITALIC        = "\247o";
    public static final String RESET         = "\247r";

    private static final Pattern STRIP_COLOR_PATTERN =
            Pattern.compile("(?i)" + String.valueOf("\247") + "[A-F0-9KLMNOR]");

    public static String stripColorCodes(final String input)
    {
        if (input == null)
        { return null; }
        return STRIP_COLOR_PATTERN.matcher(input).replaceAll("");
    }
}
