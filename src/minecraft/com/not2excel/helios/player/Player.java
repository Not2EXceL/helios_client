package com.not2excel.helios.player;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/22/13
 */
public class Player
{
    private final String     label;
    private       String     alias;
    private       PlayerType type;
    private       int        color;

    public Player(final String label, final String alias, final PlayerType type)
    {
        this.label = label;
        this.alias = alias;
        this.type = type;
    }

    //region <getters/setters>
    public String getLabel()
    {
        return label;
    }

    public String getAlias()
    {
        return alias;
    }

    public void setAlias(final String alias)
    {
        this.alias = alias;
    }

    public PlayerType getType()
    {
        return type;
    }

    public void setType(final PlayerType type)
    {
        this.type = type;
    }

    public int getColor()
    {
        return color;
    }

    public void setColor(final int color)
    {
        this.color = color;
    }
    //endregion
}
