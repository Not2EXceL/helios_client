package com.not2excel.helios.player;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/22/13
 */
public enum PlayerType
{
    FRIEND(0xFF5f0eb0),
    ENEMY(0xFFff0073);

    private final int color;

    private PlayerType(final int color)
    {
        this.color = color;
    }

    public int getColor()
    {
        return color;
    }
}
