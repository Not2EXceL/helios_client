package com.not2excel.helios.gui.menusystem;

import com.not2excel.helios.client.Utilities;
import com.not2excel.helios.client.Variables;
import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.gui.menusystem.container.NewsContainer;
import com.not2excel.helios.gui.menusystem.menu.SinglePlayerMenu;
import com.not2excel.helios.gui.screen.ScreenHeliosOptions;
import com.not2excel.helios.util.color.ColorHelper;
import com.not2excel.helios.util.drawing.Drawing2D;
import com.not2excel.helios.util.drawing.GL11Assist;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Project;

import java.awt.*;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/7/13
 */
public class HeliosMenu extends GuiScreen
{
    private static final ResourceLocation[] titlePanoramaPaths = new ResourceLocation[]{new ResourceLocation(
            "textures/gui/title/background/panorama_0.png"), new ResourceLocation(
            "textures/gui/title/background/panorama_1.png"), new ResourceLocation(
            "textures/gui/title/background/panorama_2.png"), new ResourceLocation(
            "textures/gui/title/background/panorama_3.png"), new ResourceLocation(
            "textures/gui/title/background/panorama_4.png"), new ResourceLocation(
            "textures/gui/title/background/panorama_5.png")};
    protected final HeliosMenu parentMenu;
    public    HeliosWindow    window          = null;
    public    FontRenderer    fontRenderer    = Wrapper.getInstance().getFontRenderer();
    protected boolean         windowShown     = false;
    protected int             startX          = 2;
    protected int             startY          = 2;
    protected int             tabWidth        = 88;
    protected int             tabHeight       = 30;
    protected int             errorAlpha      = 0;
    protected boolean         errorBox        = false;
    protected String          errorMessage    = "";
    protected HeliosContainer container       = null;
    private   int             transitionAlpha = 255;
    private ResourceLocation field_110351_G;
    private DynamicTexture   viewportTexture;
    private int              panoramaTimer;

    public HeliosMenu(final HeliosMenu parentMenu)
    {
        this.parentMenu = parentMenu;
    }

    public void initGui()
    {
        initBaseMenuStuff();
        buttonList.add(new HeliosTabButton(0, mc.displayWidth / 2 - tabWidth - 3,
                                           buttonList.size() * (tabHeight + 1) + 3, tabWidth,
                                           tabHeight, I18n.func_135053_a("menu.singleplayer")));
        buttonList.add(new HeliosTabButton(1, mc.displayWidth / 2 - tabWidth - 3,
                                           buttonList.size() * (tabHeight + 1) + 3, tabWidth,
                                           tabHeight, I18n.func_135053_a("menu.multiplayer")));
        buttonList.add(new HeliosTabButton(2, mc.displayWidth / 2 - tabWidth - 3,
                                           buttonList.size() * (tabHeight + 1) + 3, tabWidth,
                                           tabHeight, I18n.func_135053_a("menu.options")));
        this.buttonList.add(new HeliosTabButton(3, mc.displayWidth / 2 - tabWidth - 3,
                                                buttonList.size() * (tabHeight + 1) + 3,
                                                tabWidth, tabHeight, I18n.func_135053_a("menu.online")));
        this.buttonList.add(new HeliosTabButton(4, mc.displayWidth / 2 - tabWidth - 3,
                                                buttonList.size() * (tabHeight + 1) + 3,
                                                tabWidth, tabHeight, "Helios Options"));
        this.buttonList.add(new HeliosTabButton(-1, mc.displayWidth / 2 - tabWidth - 3,
                                                buttonList.size() * (tabHeight + 1) + 3,
                                                tabWidth, tabHeight, I18n.func_135053_a("menu.quit")));
        if (container == null)
        {
            container = new NewsContainer(0, startX, startY + 80, mc.displayWidth / 2 - tabWidth - 3.5f,
                                          mc.displayHeight / 2 - 2, this);
        }
    }

    protected void initBaseMenuStuff()
    {
        this.viewportTexture = new DynamicTexture(256, 256);
        this.field_110351_G = this.mc.func_110434_K().func_110578_a("background", this.viewportTexture);
        if (transitionAlpha == 0)
        { transitionAlpha = 255; }
    }

    @Override
    public void updateScreen()
    {
        transitionAlpha -= 10;
        if (transitionAlpha <= 0)
        { transitionAlpha = 0; }
        if (errorBox && errorAlpha <= 255)
        { errorAlpha += 20; }
        else
        { errorAlpha -= 22; }
        if (errorAlpha >= 255)
        { errorAlpha = 255; }
        if (errorAlpha <= 0)
        { errorAlpha = 0; }
        if (window != null)
        {
            if (windowShown && window.windowAlpha <= 255)
            { window.windowAlpha += 20; }
            else
            { window.windowAlpha -= 22; }
            if (window.windowAlpha >= 255)
            { window.windowAlpha = 255; }
            if (window.windowAlpha <= 0)
            { window.windowAlpha = 0; }
        }
        ++this.panoramaTimer;
    }

    @Override
    protected void actionPerformed(GuiButton button)
    {
        if (button.id == -1)
        { mc.shutdown(); }
        if (button.id == 0)
        { mc.displayGuiScreen(new SinglePlayerMenu(this)); }
        if (button.id == 1)
        { mc.displayGuiScreen(new GuiMultiplayer(this)); }
        if (button.id == 2)
        { mc.displayGuiScreen(new GuiOptions(this, Wrapper.getInstance().getGameSettings())); }
        if (button.id == 3)
        { initializeRealms(); }
        if (button.id == 4)
        { mc.displayGuiScreen(new ScreenHeliosOptions(this)); }
    }

    @Override
    public void drawScreen(int par1, int par2, float par3)
    {
        renderBase(par1, par2, par3);

        //TITLE STUFF
        String title = String.format("%s v%s", Variables.getInstance().CLIENT_TITLE,
                                     Variables.getInstance().CLIENT_VERSION);
        fontRenderer.drawStringWithShadow(title, mc.displayWidth / 2 - 90,
                                          buttonList.size() * (tabHeight + 1) + 4, 0xFF004bff);
        fontRenderer.drawStringWithShadow("Minecraft 1.6.2", mc.displayWidth / 2 - 90,
                                          buttonList.size() * (tabHeight + 1) + 4 +
                                          Wrapper.getInstance().getFontRenderer().stringCache.getStringHeight() + 2,
                                          0xFF8a8a8a);
        List<String> updateStrings = Utilities.getInstance().listFormattedStringToWidth(
                Utilities.getInstance().clientUpdateString(), tabWidth - 8);
        for (int i = 0; i < updateStrings.size(); i++)
        {
            String update = updateStrings.get(i);
            fontRenderer.drawStringWithShadow(update, mc.displayWidth / 2 - 90,
                                              buttonList.size() * (tabHeight + 1) + 4 +
                                              (Wrapper.getInstance().getFontRenderer().stringCache.getStringHeight() +
                                               2) * (2 + i), 0xFFffffff);
        }

        renderFade();
    }

    private void renderBase(int par1, int par2, float par3)
    {
        renderSkybox(par1, par2, par3);
        String watermark = "An InBread Studios Development - Not2EXceL";
        fontRenderer.drawString(watermark, 4, mc.displayHeight / 2 - fontRenderer.stringCache.getStringHeight() - 4,
                                0x9a1a1a1a);
//        Drawing2D.drawRect(mc.displayWidth / 2 - tabWidth - 3.5f, startY, mc.displayWidth / 2 - 2,
//                           mc.displayHeight / 2 - 2, 0x60111111);
        super.drawScreen(par1, par2, par3);
        if (container != null)
        { container.drawContainer(mc, par1, par2); }
        if (window != null && windowShown)
        { window.drawWindow(startX, startY); }
    }

    private void renderFade()
    {
        GL11Assist.defaultsOnGui();
        if (transitionAlpha > 0)
        {
            Drawing2D.drawRect(0, 0, mc.displayWidth / 2, mc.displayHeight / 2,
                               (new Color(0, 0, 0, transitionAlpha)).getRGB());
        }
        if (errorBox || errorAlpha != 0)
        { displayErrorBox(errorMessage); }
        GL11Assist.defaultsOffGui();
    }

    @Override
    protected void mouseClicked(int par1, int par2, int par3)
    {
        if (errorBox)
        {
            if (par1 >= mc.displayWidth / 4 - 50 - tabWidth &&
                par1 <= mc.displayWidth / 4 + 50 &&
                par2 >= mc.displayHeight / 4 - 50 &&
                par2 <= mc.displayHeight / 4 + 50)
            { errorBox = false; }
        }
        else if (window != null && windowShown)
        { window.mouseClicked(par1, par2, par3); }
        else
        {
            if (container != null)
            {
                if (container.isOverContainer(par1, par2))
                { container.mouseClicked(par1, par2, par3); }
            }
            super.mouseClicked(par1, par2, par3);
        }
    }

    @Override
    protected void keyTyped(char par1, int par2)
    {
        if (container != null)
        { container.keyTyped(par1, par2); }
        super.keyTyped(par1, par2);
    }

    @Override
    protected void mouseMovedOrUp(int par1, int par2, int par3)
    {
        if (container != null)
        { container.mouseDragged(par1, par2, par3); }
        super.mouseMovedOrUp(par1, par2, par3);
    }

    private void initializeRealms()
    {
        McoClient var1 = new McoClient(mc.func_110432_I());
        try
        {
            if (var1.func_140054_c().booleanValue())
            { mc.displayGuiScreen(new GuiScreenClientOutdated(this)); }
            else
            { mc.displayGuiScreen(new GuiScreenOnlineServers(this)); }
        }
        catch (ExceptionMcoService var3)
        {
            mc.getLogAgent().logSevere(var3.toString());
            errorBox = true;
            errorMessage = var3.toString();
        }
        catch (IOException var4)
        {
            this.mc.getLogAgent().logSevere(var4.getLocalizedMessage());
            errorBox = true;
            errorMessage = var4.getLocalizedMessage();
        }
    }

    private void displayErrorBox(final String input)
    {
        List<String> inputList = Utilities.getInstance().listFormattedStringToWidth(input, tabWidth + 100 - 8);
        Drawing2D.drawBorderedRect(mc.displayWidth / 4 - 50 - tabWidth, mc.displayHeight / 4 - 50,
                                   mc.displayWidth / 4 + 50,
                                   mc.displayHeight / 4 + 50,
                                   (new Color(255, 255, 255, (errorAlpha >= 128) ? 128 : errorAlpha)).getRGB(),
                                   (new Color(16, 16, 16, (errorAlpha >= 207) ? 207 : errorAlpha)).getRGB());
        Drawing2D.drawBorderedRect(mc.displayWidth / 4 - 50 - tabWidth - 0.5f, mc.displayHeight / 4 - 50 - 0.5f,
                                   mc.displayWidth / 4 + 50 + 0.5f,
                                   mc.displayHeight / 4 + 50 + 0.25f,
                                   (new Color(91, 91, 91, errorAlpha)).getRGB(), 0x00ffffff);
        Drawing2D.drawHLine(mc.displayWidth / 4 - 50 - tabWidth + 0.5f,
                            mc.displayWidth / 4 + 49 - 0.5f,
                            mc.displayHeight / 4 - 50 +
                            (fontRenderer.stringCache.getStringHeight() + 2) * 2 - 0.75f,
                            (new Color(91, 91, 91, errorAlpha)).getRGB());
        Drawing2D.drawHLine(mc.displayWidth / 4 - 50 - tabWidth + 0.5f,
                            mc.displayWidth / 4 + 49 - 0.5f,
                            mc.displayHeight / 4 - 50 +
                            (fontRenderer.stringCache.getStringHeight() + 2) * 2,
                            (new Color(255, 255, 255, errorAlpha)).getRGB());
        Drawing2D.drawHLine(mc.displayWidth / 4 - 50 - tabWidth + 0.5f,
                            mc.displayWidth / 4 + 49 - 0.5f,
                            mc.displayHeight / 4 - 50 +
                            (fontRenderer.stringCache.getStringHeight() + 2) * 2 + 0.25f,
                            (new Color(91, 91, 91, errorAlpha)).getRGB());
        fontRenderer.drawStringWithShadow(ColorHelper.RED + "Error Message",
                                          (mc.displayWidth / 4 + mc.displayWidth / 4) / 2 - tabWidth / 2 -
                                          fontRenderer.getStringWidth("Error Message") / 2,
                                          mc.displayHeight / 4 - 50 + fontRenderer.stringCache.getStringHeight() - 2,
                                          (new Color(255, 255, 255, errorAlpha)).getRGB());
        for (int i = 0; i < inputList.size(); i++)
        {
            String draw = inputList.get(i);
            fontRenderer.drawStringWithShadow(draw,
                                              (mc.displayWidth / 4 + mc.displayWidth / 4) / 2 - tabWidth / 2 -
                                              fontRenderer.getStringWidth(draw) / 2,
                                              mc.displayHeight / 4 - 50 +
                                              (fontRenderer.stringCache.getStringHeight() + 2) * (2 + i) + 4,
                                              (new Color(255, 255, 255, errorAlpha)).getRGB());
        }
        fontRenderer.drawStringWithShadow(ColorHelper.YELLOW + "Click This Box To Close",
                                          (mc.displayWidth / 4 + mc.displayWidth / 4) / 2 - tabWidth / 2 -
                                          fontRenderer.getStringWidth("Click This Box To Close") / 2,
                                          mc.displayHeight / 4 + 50 - fontRenderer.stringCache.getStringHeight() - 4,
                                          (new Color(255, 255, 255, errorAlpha)).getRGB());
    }


    //------------------------------------------------------------------------

    /**
     * Draws the main menu panorama
     */
    private void drawPanorama(int par1, int par2, float par3)
    {
        Tessellator var4 = Tessellator.instance;
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        Project.gluPerspective(120.0F, 1.0F, 0.05F, 10.0F);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glDisable(GL11.GL_CULL_FACE);
        GL11.glDepthMask(false);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        byte var5 = 8;

        for (int var6 = 0; var6 < var5 * var5; ++var6)
        {
            GL11.glPushMatrix();
            float var7 = ((float) (var6 % var5) / (float) var5 - 0.5F) / 64.0F;
            float var8 = ((float) (var6 / var5) / (float) var5 - 0.5F) / 64.0F;
            float var9 = 0.0F;
            GL11.glTranslatef(var7, var8, var9);
            GL11.glRotatef(MathHelper.sin(((float) this.panoramaTimer + par3) / 400.0F) * 25.0F + 20.0F, 1.0F, 0.0F,
                           0.0F);
            GL11.glRotatef(-((float) this.panoramaTimer + par3) * 0.1F, 0.0F, 1.0F, 0.0F);

            for (int var10 = 0; var10 < 6; ++var10)
            {
                GL11.glPushMatrix();

                if (var10 == 1)
                {
                    GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
                }

                if (var10 == 2)
                {
                    GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
                }

                if (var10 == 3)
                {
                    GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
                }

                if (var10 == 4)
                {
                    GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
                }

                if (var10 == 5)
                {
                    GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
                }

                this.mc.func_110434_K().func_110577_a(titlePanoramaPaths[var10]);
                var4.startDrawingQuads();
                var4.setColorRGBA_I(16777215, 255 / (var6 + 1));
                float var11 = 0.0F;
                var4.addVertexWithUV(-1.0D, -1.0D, 1.0D, (double) (0.0F + var11), (double) (0.0F + var11));
                var4.addVertexWithUV(1.0D, -1.0D, 1.0D, (double) (1.0F - var11), (double) (0.0F + var11));
                var4.addVertexWithUV(1.0D, 1.0D, 1.0D, (double) (1.0F - var11), (double) (1.0F - var11));
                var4.addVertexWithUV(-1.0D, 1.0D, 1.0D, (double) (0.0F + var11), (double) (1.0F - var11));
                var4.draw();
                GL11.glPopMatrix();
            }

            GL11.glPopMatrix();
            GL11.glColorMask(true, true, true, false);
        }

        var4.setTranslation(0.0D, 0.0D, 0.0D);
        GL11.glColorMask(true, true, true, true);
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPopMatrix();
        GL11.glDepthMask(true);
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
    }

    /**
     * Rotate and blurs the skybox view in the main menu
     */
    private void rotateAndBlurSkybox(float par1)
    {
        Wrapper.getInstance().getRenderEngine().func_110577_a(field_110351_G);
        GL11.glCopyTexSubImage2D(GL11.GL_TEXTURE_2D, 0, 0, 0, 0, 0, 256, 256);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColorMask(true, true, true, false);
        Tessellator var2 = Tessellator.instance;
        var2.startDrawingQuads();
        byte var3 = 3;

        for (int var4 = 0; var4 < var3; ++var4)
        {
            var2.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F / (float) (var4 + 1));
            int var5 = this.width;
            int var6 = this.height;
            float var7 = (float) (var4 - var3 / 2) / 256.0F;
            var2.addVertexWithUV((double) var5, (double) var6, (double) this.zLevel, (double) (0.0F + var7), 0.0D);
            var2.addVertexWithUV((double) var5, 0.0D, (double) this.zLevel, (double) (1.0F + var7), 0.0D);
            var2.addVertexWithUV(0.0D, 0.0D, (double) this.zLevel, (double) (1.0F + var7), 1.0D);
            var2.addVertexWithUV(0.0D, (double) var6, (double) this.zLevel, (double) (0.0F + var7), 1.0D);
        }

        var2.draw();
        GL11.glColorMask(true, true, true, true);
    }

    /**
     * Renders the skybox in the main menu
     */
    private void renderSkybox(int par1, int par2, float par3)
    {
        GL11.glViewport(0, 0, 256, 256);
        this.drawPanorama(par1, par2, par3);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        this.rotateAndBlurSkybox(par3);
        this.rotateAndBlurSkybox(par3);
        this.rotateAndBlurSkybox(par3);
        this.rotateAndBlurSkybox(par3);
        this.rotateAndBlurSkybox(par3);
        this.rotateAndBlurSkybox(par3);
        this.rotateAndBlurSkybox(par3);
        this.rotateAndBlurSkybox(par3);
        GL11.glViewport(0, 0, this.mc.displayWidth, this.mc.displayHeight);
        Tessellator var4 = Tessellator.instance;
        var4.startDrawingQuads();
        float var5 = this.width > this.height ? 120.0F / (float) this.width : 120.0F / (float) this.height;
        float var6 = (float) this.height * var5 / 256.0F;
        float var7 = (float) this.width * var5 / 256.0F;
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        var4.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F);
        int var8 = this.width;
        int var9 = this.height;
        var4.addVertexWithUV(0.0D, (double) var9, (double) this.zLevel, (double) (0.5F - var6), (double) (0.5F + var7));
        var4.addVertexWithUV((double) var8, (double) var9, (double) this.zLevel, (double) (0.5F - var6),
                             (double) (0.5F - var7));
        var4.addVertexWithUV((double) var8, 0.0D, (double) this.zLevel, (double) (0.5F + var6), (double) (0.5F - var7));
        var4.addVertexWithUV(0.0D, 0.0D, (double) this.zLevel, (double) (0.5F + var6), (double) (0.5F + var7));
        var4.draw();
    }
}