package com.not2excel.helios.gui.menusystem.slot;

import com.not2excel.helios.gui.menusystem.HeliosContainer;
import com.not2excel.helios.gui.menusystem.HeliosSlot;
import com.not2excel.helios.gui.menusystem.menu.SinglePlayerMenu;
import com.not2excel.helios.util.drawing.Drawing2D;
import com.not2excel.helios.util.drawing.GL11Assist;
import net.minecraft.src.*;
import org.lwjgl.util.vector.Vector2f;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/10/13
 */
public class SinglePlayerSlot extends HeliosSlot
{
    protected Vector2f selectedWorld         = new Vector2f(-1, -1);
    protected String[] localizedGameModeText = new String[3];
    protected String localizedWorldText;
    protected String localizedMustConvertText;

    public SinglePlayerSlot(HeliosContainer parentContainer)
    {
        super(parentContainer);
        try
        {
            ISaveFormat var1 = parentContainer.mc.getSaveLoader();
            this.list = var1.getSaveList();
            Collections.sort(this.list);
            selected = -1;
        }
        catch (Exception e)
        { e.printStackTrace(); }
        this.localizedWorldText = I18n.func_135053_a("selectWorld.world");
        this.localizedMustConvertText = I18n.func_135053_a("selectWorld.conversion");
        this.localizedGameModeText[EnumGameType.SURVIVAL.getID()] = I18n.func_135053_a("gameMode.survival");
        this.localizedGameModeText[EnumGameType.CREATIVE.getID()] = I18n.func_135053_a("gameMode.creative");
        this.localizedGameModeText[EnumGameType.ADVENTURE.getID()] = I18n.func_135053_a("gameMode.adventure");
    }

    @Override
    protected void renderDrawingAndStrings(int x, int y, int i, int j)
    {
        boolean selected = selectedWorld.getX() == i && selectedWorld.getY() == j;
        GL11Assist.defaultsOnGui();
        Drawing2D.drawBorderedRect((float) slotX, (float) slotY, (float) slotWidth, (float) slotHeight,
                                   0x00ffffff, selected ? 0x7c1a1a1a : 0x00ffffff);
        Drawing2D.drawGradientBorderedRect((float) slotX + 0.5, (float) slotHeight - 4.5,
                                           (float) slotWidth - 0.5, (float) slotHeight, 1,
                                           0x00ffffff,
                                           selected ? 0xff7D00D1 : 0x00ffffff,
                                           selected ? 0xff2B0047 : 0x00ffffff);
        if (isMouseOver(x, y, slotX, slotY, slotWidth, slotHeight) && !selected)
        {
            Drawing2D.drawBorderedRect((float) slotX, (float) slotY, (float) slotWidth, (float) slotHeight,
                                       0x00ffffff, 0x7c1a1a1a);
        }
        GL11Assist.defaultsOffGui();
        String worldName = ((SaveFormatComparator) list.get(i)).getDisplayName();
        if (worldName == null || MathHelper.stringNullOrLengthZero(worldName))
        { worldName = this.localizedWorldText + " " + (i + 1); }
        String worldLocation = ((SaveFormatComparator) list.get(i)).getFileName();
        String worldLastAccessed = "(" + new SimpleDateFormat().format(new Date(
                ((SaveFormatComparator) list.get(i)).getLastTimePlayed())) + ")";
        String worldMode = "";
        if (((SaveFormatComparator) list.get(i)).requiresConversion())
        { worldMode = localizedMustConvertText + " " + worldMode; }
        else
        {
            worldMode = localizedGameModeText[((SaveFormatComparator) list.get(i)).getEnumGameType().getID()];
            if (((SaveFormatComparator) list.get(i)).isHardcoreModeEnabled())
            {
                worldMode = EnumChatFormatting.DARK_RED + I18n.func_135053_a("gameMode.hardcore") +
                            EnumChatFormatting.RESET;
            }
            if (((SaveFormatComparator) list.get(i)).getCheatsEnabled())
            { worldMode = worldMode + ", " + I18n.func_135053_a("selectWorld.cheats"); }
        }

        parentContainer.fontRenderer.drawStringWithShadow(String.valueOf(i), (int) slotX + 2, (int) slotY + 2, 0xFFffffff);
        parentContainer.fontRenderer.drawStringWithShadow(worldLocation, (int) slotX + 2, (int) slotY + 12, 0xFFffffff);
        parentContainer.fontRenderer.drawStringWithShadow(worldLastAccessed,
                                                          (int) slotX + 2, (int) slotY + 22, 0xFFffffff);
        parentContainer.fontRenderer.drawStringWithShadow(worldMode, (int) slotX + 2, (int) slotY + 32, 0xFFffffff);
    }

    @Override
    protected void checkMouseClicked(final int par1, final int par2, final int par3, final int i, final int j)
    {
        if (par1 >= slotMouseX &&
            par2 >= slotMouseY &&
            par1 <= slotMouseWidth &&
            par2 <= slotMouseHeight &&
            par3 == 0)
        {
            selectedWorld = new Vector2f(i, j);
            setSelected(i);
            ((SinglePlayerMenu)parentContainer.parentMenu).enableButtons();
            return;
        }
    }
}
