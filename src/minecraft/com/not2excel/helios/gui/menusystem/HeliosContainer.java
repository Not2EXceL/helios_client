package com.not2excel.helios.gui.menusystem;

import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.util.drawing.GL11Assist;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.Gui;
import net.minecraft.src.Minecraft;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/7/13
 */
public abstract class HeliosContainer extends Gui
{
    public int          id            = -1;
    public float        startX        = 0;
    public float        startY        = 0;
    public float        width         = 0;
    public float        height        = 0;
    public float        scissorX      = 0;
    public float        scissorY      = 0;
    public float        scissorWidth  = 0;
    public float        scissorHeight = 0;
    public HeliosMenu   parentMenu    = null;
    public FontRenderer fontRenderer  = Wrapper.getInstance().getFontRenderer();
    public Minecraft    mc            = Wrapper.getInstance().getMinecraft();

    public HeliosContainer(int id, float x, float y, float width, float height, HeliosMenu parentMenu)
    {
        this.id = id;
        this.startX = x;
        this.startY = y;
        this.width = width;
        this.height = height;
        this.scissorX = x - 2;
        this.scissorY = y;
        this.scissorWidth = width + 2;
        this.scissorHeight = height;
        this.parentMenu = parentMenu;
    }

    public void drawContainer(Minecraft mc, int x, int y)
    {
        if (fontRenderer != mc.fontRenderer)
        { fontRenderer = mc.fontRenderer; }
        GL11Assist.enableScissor();
        GL11Assist.prepareScissorBox(scissorX, scissorY, scissorWidth, scissorHeight);
        drawContainerStuff(mc, x, y);
        GL11Assist.disableScissor();
    }

    protected abstract void drawContainerStuff(Minecraft mc, int x, int y);

    protected void mouseClicked(int par1, int par2, int par3)
    {
        mouseClickedCheck(par1, par2, par3);
    }

    protected abstract void mouseClickedCheck(int par1, int par2, int par3);

    protected void mouseDragged(int par1, int par2, int par3) {}

    protected void keyTyped(char par1, int par2) {}

    public boolean isOverContainer(int x, int y)
    {
        return x >= startX &&
               x <= startX + width &&
               y >= startY &&
               y <= startY + height;
    }
}
