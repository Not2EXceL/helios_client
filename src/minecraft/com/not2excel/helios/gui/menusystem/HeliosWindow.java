package com.not2excel.helios.gui.menusystem;

import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.util.drawing.Drawing2D;
import com.not2excel.helios.util.drawing.GL11Assist;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.Gui;
import net.minecraft.src.Minecraft;

import java.awt.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/13/13
 */
public abstract class HeliosWindow extends Gui
{
    protected final HeliosMenu   parentMenu;
    protected final FontRenderer fontRenderer;
    public          int          selected;
    public          List         list;
    protected int windowAlpha = 0;
    protected float  tabWidth;
    protected Minecraft mc = Wrapper.getInstance().getMinecraft();

    public HeliosWindow(final HeliosMenu parentMenu)
    {
        this.parentMenu = parentMenu;
        fontRenderer = parentMenu.fontRenderer;
        tabWidth = parentMenu.tabWidth;
    }

    public void drawWindow(final float x, final float y)
    {
        GL11Assist.defaultsOnGui();
        Drawing2D.drawBorderedRect(mc.displayWidth / 4 - 50 - tabWidth, mc.displayHeight / 4 - 50,
                                   mc.displayWidth / 4 + 50,
                                   mc.displayHeight / 4 + 50,
                                   (new Color(255, 255, 255, (windowAlpha >= 128) ? 128 : windowAlpha)).getRGB(),
                                   (new Color(16, 16, 16, (windowAlpha >= 207) ? 207 : windowAlpha)).getRGB());
        Drawing2D.drawBorderedRect(mc.displayWidth / 4 - 50 - tabWidth - 0.5f, mc.displayHeight / 4 - 50 - 0.5f,
                                   mc.displayWidth / 4 + 50 + 0.5f,
                                   mc.displayHeight / 4 + 50 + 0.25f,
                                   (new Color(91, 91, 91, windowAlpha)).getRGB(), 0x00ffffff);
        Drawing2D.drawHLine(mc.displayWidth / 4 - 50 - tabWidth + 0.5f,
                            mc.displayWidth / 4 + 49 - 0.5f,
                            mc.displayHeight / 4 - 50 +
                            (fontRenderer.stringCache.getStringHeight() + 2) * 2 - 0.75f,
                            (new Color(91, 91, 91, windowAlpha)).getRGB());
        Drawing2D.drawHLine(mc.displayWidth / 4 - 50 - tabWidth + 0.5f,
                            mc.displayWidth / 4 + 49 - 0.5f,
                            mc.displayHeight / 4 - 50 +
                            (fontRenderer.stringCache.getStringHeight() + 2) * 2,
                            (new Color(255, 255, 255, windowAlpha)).getRGB());
        Drawing2D.drawHLine(mc.displayWidth / 4 - 50 - tabWidth + 0.5f,
                            mc.displayWidth / 4 + 49 - 0.5f,
                            mc.displayHeight / 4 - 50 +
                            (fontRenderer.stringCache.getStringHeight() + 2) * 2 + 0.25f,
                            (new Color(91, 91, 91, windowAlpha)).getRGB());
        GL11Assist.defaultsOffGui();
        renderDrawing(x, y);
    }

    protected abstract void renderDrawing(final float x, final float y);

    protected abstract void checkMouseClicked(final int par1, final int par2, final int par3);

    public void mouseClicked(final int par1, final int par2, final int par3)
    {
        checkMouseClicked(par1, par2, par3);
    }
}
