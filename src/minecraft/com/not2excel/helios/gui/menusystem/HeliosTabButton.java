package com.not2excel.helios.gui.menusystem;

import com.not2excel.helios.client.Utilities;
import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.util.drawing.Drawing2D;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.GuiButton;
import net.minecraft.src.Minecraft;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/7/13
 */
public class HeliosTabButton extends GuiButton
{
    private long lastScale;
    private int scaledWidth  = 0;
    private int scaledHeight = 0;
    private int red          = 0;
    private int green        = 0;
    private int blue         = 0;
    private int green2       = 0;
    private int blue2        = 0;
    private int alpha        = 0;

    public HeliosTabButton(int par1, int par2, int par3, int par4, int par5, String par6Str)
    {
        super(par1, par2, par3, par4, par5, par6Str);
    }

    /**
     * Draws this button to the screen.
     */
    public void drawButton(Minecraft par1Minecraft, int par2, int par3)
    {
        if (this.drawButton)
        {
            FontRenderer var4 = par1Minecraft.fontRenderer;
            this.updateButton();
            Color topColor = new Color(red, green, blue, 200);
            Color bottomColor = new Color(red, green2, blue2, 200);
            Color border = new Color(red, green2, blue2, alpha);
            Color border2 = new Color(25, 25, 25, 255);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.isOverButton =
                    par2 >= this.xPosition && par3 >= this.yPosition && par2 < this.xPosition + this.width &&
                    par3 < this.yPosition + this.height;
            GL11.glEnable(GL11.GL_BLEND);
//            Drawing2D.drawGradientBorderedRect(this.xPosition - this.scaledWidth, this.yPosition - this.scaledHeight,
//                                               this.xPosition + this.width + this.scaledWidth,
//                                               this.yPosition + this.height + this.scaledHeight, 1,
//                                               border.getRGB(), bottomColor.getRGB(), topColor.getRGB());
//            Drawing2D.drawGradientBorderedRect(this.xPosition - this.scaledWidth, this.yPosition - this.scaledHeight,
//                                               this.xPosition + this.width + this.scaledWidth,
//                                               this.yPosition + this.height + this.scaledHeight, 1,
//                                               border.getRGB(), topColor.getRGB(), bottomColor.getRGB());
            Drawing2D.drawBorderedRect(this.xPosition - this.scaledWidth - 0.5f,
                                       this.yPosition - this.scaledHeight - 0.5f,
                                       this.xPosition + this.width + this.scaledWidth + 0.5f,
                                       this.yPosition + this.height + this.scaledHeight + 0.25f,
                                       border2.getRGB(),
                                       border.getRGB());
            GL11.glDisable(GL11.GL_BLEND);
            this.mouseDragged(par1Minecraft, par2, par3);
            int var6 = 0xFF9f9f9f;

            if (!this.enabled)
            { var6 = 0xff282829; }
            else if (this.isOverButton)
            { var6 = 0xFFffffff; }

            List<String> list = Utilities.getInstance().listFormattedStringToWidth(this.displayString, this.width);
            if (list.size() == 1)
            {
                this.drawCenteresdString(var4, list.get(0), this.xPosition + this.width / 2,
                                         this.yPosition + (this.height - 10) / 2, var6, enabled);
            }
            else
            {
                int i = 0;
                for (String s : list)
                {
                    this.drawCenteresdString(var4, s, this.xPosition + this.width / 2,
                                             this.yPosition + (this.height - 10 - (list.size() * list.size()) * 2) / 2 +
                                             i * 10, var6, enabled);
                    i++;
                }
            }
        }
    }

    public void drawCenteresdString(FontRenderer par1FontRenderer, String par2Str, int par3, int par4, int par5,
                                    boolean shadow)
    {
        if (shadow)
        {
            Wrapper.getInstance().getFontRenderer().drawStringWithShadow(par2Str,
                                                                         (par3 - Wrapper.getInstance().getFontRenderer()
                                                                                        .getStringWidth(par2Str) / 2),
                                                                         par4,
                                                                         par5);
        }
        else
        {
            Wrapper.getInstance().getFontRenderer().drawString(par2Str,
                                                               (par3 - Wrapper.getInstance().getFontRenderer()
                                                                              .getStringWidth(par2Str) / 2), par4,
                                                               par5);
        }
    }

    private void updateButton()
    {
        if (System.nanoTime() / 1000000 - this.lastScale >= 15L)
        {
            if (isOverButton && this.enabled)
            {
                green -= 4;
                blue -= 12;
                green2 += 4;
                blue2 += 12;
                alpha += 5;
            }
            else
            {
                green += 4;
                blue += 12;
                green2 -= 4;
                blue2 -= 12;
                alpha -= 5;
            }
            this.lastScale = System.nanoTime() / 1000000;
        }
        if (green > 75)
        { green = 75; }
        if (green < 19)
        { green = 19; }
        if (blue > 255)
        { blue = 255; }
        if (blue < 64)
        { blue = 64; }
        if (green2 > 75)
        { green2 = 75; }
        if (green2 < 19)
        { green2 = 19; }
        if (blue2 > 255)
        { blue2 = 255; }
        if (blue2 < 64)
        { blue2 = 64; }
        if (alpha < 75)
        { alpha = 75; }
        if (alpha > 175)
        { alpha = 175; }
    }
}
