package com.not2excel.helios.gui.menusystem;

import com.not2excel.helios.util.drawing.Drawing2D;
import net.minecraft.src.Gui;
import net.minecraft.src.MathHelper;
import org.lwjgl.input.Mouse;

import java.awt.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/10/13
 */
public abstract class HeliosSlot extends Gui
{
    protected final HeliosContainer parentContainer;
    public          int             selected;
    public          List            list;
    protected double columnWidth = 160;
    protected double rowHeight   = 40;
    protected int    numberOfColumns;
    protected int    numberOfRows;
    protected double slotX;
    protected double slotY;
    protected double slotWidth;
    protected double slotHeight;
    protected double slotMouseX;
    protected double slotMouseY;
    protected double slotMouseWidth;
    protected double slotMouseHeight;
    private   double scrollMaxHeight;

    public HeliosSlot(final HeliosContainer parentContainer)
    {
        this.parentContainer = parentContainer;
    }

    public void drawSlot(final int x, final int y)
    {
        int dWheel = Mouse.getDWheel();
        if (dWheel < 0)
        { parentContainer.startY -= 15; }
        else if (dWheel > 0)
        { parentContainer.startY += 15; }
        numberOfColumns = MathHelper.floor_double((parentContainer.width - 10) / columnWidth);
        columnWidth = (parentContainer.width - 10) / numberOfColumns;
        numberOfRows = MathHelper.floor_double((parentContainer.height - 4) / rowHeight);
        rowHeight = (parentContainer.height - 4) / numberOfRows;
        scrollMaxHeight = numberOfRows * rowHeight;
        int i = 0;
        int j = 0;
        for (Object o : list)
        {
            slotX = (parentContainer.startX + (i % numberOfColumns) * columnWidth +
                     (i % numberOfColumns != 0 ? 2 : -3));
            slotY = (parentContainer.startY + 1.25 + j * rowHeight);
            slotWidth = (parentContainer.startX + (i % numberOfColumns) * columnWidth + columnWidth + 2.5);
            slotHeight = (parentContainer.startY + j * rowHeight + rowHeight);
            renderDrawingAndStrings(x, y, i, j);
            i++;
            if (i % numberOfColumns == 0)
            { j++; }
        }
        Drawing2D.drawRect(parentContainer.width - 6, parentContainer.startY + 40, parentContainer.width,
                           parentContainer.startY + 80, new Color(122, 0, 255).getRGB());
    }

    protected abstract void renderDrawingAndStrings(final int x, final int y, final int i, final int j);

    protected abstract void checkMouseClicked(final int par1, final int par2, final int par3, final int i, final int j);

    public void mouseClicked(final int par1, final int par2, final int par3)
    {
        int i = 0;
        int j = 0;
        for (Object o : list)
        {
            slotMouseX = (parentContainer.startX + (i % numberOfColumns) * columnWidth +
                          (i % numberOfColumns != 0 ? 2 : -3));
            slotMouseY = (parentContainer.startY + 1.25 + j * rowHeight);
            slotMouseWidth = (parentContainer.startX + (i % numberOfColumns) * columnWidth + columnWidth + 2.5);
            slotMouseHeight = (parentContainer.startY + j * rowHeight + rowHeight);
            checkMouseClicked(par1, par2, par3, i, j);
            i++;
            if (i % numberOfColumns == 0)
            { j++; }
        }
    }

    protected void setSelected(final int i)
    {
        selected = i;
    }

    protected boolean isMouseOver(int mouseX, int mouseY, double checkX, double checkY, double checkWidth,
                                  double checkHeight)
    {
        return mouseX >= checkX &&
               mouseX <= checkWidth &&
               mouseY >= checkY &&
               mouseY <= checkHeight;
    }
}
