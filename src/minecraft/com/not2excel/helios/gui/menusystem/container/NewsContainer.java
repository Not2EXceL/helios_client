package com.not2excel.helios.gui.menusystem.container;

import com.not2excel.helios.gui.menusystem.HeliosContainer;
import com.not2excel.helios.gui.menusystem.HeliosMenu;
import com.not2excel.helios.util.color.ColorHelper;
import com.not2excel.helios.util.drawing.Drawing2D;
import com.not2excel.helios.util.drawing.GL11Assist;
import com.not2excel.lib.io.URLManager;
import net.minecraft.src.Minecraft;

import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/8/13
 */
public class NewsContainer extends HeliosContainer
{
    private List<String> newsList = null;

    public NewsContainer(int id, float x, float y, float width, float height, HeliosMenu menu)
    {
        super(id, x, y, width, height, menu);
        if (newsList == null)
        { newsList = getNews(); }
        if (newsList.isEmpty())
        { newsList.add(ColorHelper.DARK_RED + "The News is BROKEDED :("); }
    }

    private synchronized List<String> getNews()
    {
        URL newsURL = null;
        List<String> tempList = new LinkedList<String>();
        try
        { newsURL = new URL("http://mc.not2excel.com/helios_news"); }
        catch (MalformedURLException e)
        { e.printStackTrace(); }
        try
        {
            URLManager urlManager = new URLManager(newsURL);
            urlManager.setupReadStream();
            String s;
            while ((s = urlManager.readLine()) != null)
            { tempList.add(s); }
        }
        catch (Exception e)
        { e.printStackTrace(); }
        return tempList;
    }

    @Override
    public void drawContainerStuff(Minecraft mc, int x, int y)
    {
        GL11Assist.defaultsOnGui();
        Drawing2D.drawBorderedRect(startX, startY, width - 2, height - 1, 0xFF1a1a1a, 0x7c1a1a1a);
        Drawing2D.drawHLine(startX + 0.5f,
                            mc.displayWidth / 2 - 95,
                            startY + (fontRenderer.stringCache.getStringHeight() + 2) * 2 - 3,
                            (new Color(91, 91, 91, 255)).getRGB());
        Drawing2D.drawHLine(startX + 0.5f,
                            mc.displayWidth / 2 - 95,
                            startY + (fontRenderer.stringCache.getStringHeight() + 2) * 2 - 2,
                            (new Color(255, 255, 255, 255)).getRGB());
        Drawing2D.drawHLine(startX + 0.5f,
                            mc.displayWidth / 2 - 95,
                            startY + (fontRenderer.stringCache.getStringHeight() + 2) * 2 - 1.75f,
                            (new Color(91, 91, 91, 255)).getRGB());
        GL11Assist.defaultsOffGui();
        fontRenderer.drawStringWithShadow("News Feed", this.startX + 4, this.startY + 4.5f, 0xFFffffff);
        for (int i = 0; i < newsList.size(); i++)
        {
            String s = newsList.get(i);
            fontRenderer.drawStringWithShadow(s, this.startX + 2, this.startY + 10 +
                                                                  ((i + 1) *
                                                                   (fontRenderer.stringCache.getStringHeight() + 2) +
                                                                   2), 0xFFffffff);
        }
    }

    @Override
    protected void mouseClickedCheck(int par1, int par2, int par3)
    {
    }
}
