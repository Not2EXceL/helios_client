package com.not2excel.helios.gui.menusystem.container;

import com.not2excel.helios.gui.menusystem.HeliosContainer;
import com.not2excel.helios.gui.menusystem.HeliosMenu;
import net.minecraft.src.Minecraft;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/9/13
 */
public class HeliosOptionsContainer extends HeliosContainer
{
    public HeliosOptionsContainer(int id, float x, float y, float width, float height, HeliosMenu menu)
    {
        super(id, x, y, width, height, menu);
    }

    @Override
    protected void drawContainerStuff(Minecraft mc, int x, int y)
    {
    }

    @Override
    protected void mouseClickedCheck(int par1, int par2, int par3)
    {
    }
}
