package com.not2excel.helios.gui.menusystem.container;

import com.not2excel.helios.gui.menusystem.HeliosContainer;
import com.not2excel.helios.gui.menusystem.HeliosMenu;
import com.not2excel.helios.gui.menusystem.HeliosSlot;
import com.not2excel.helios.gui.menusystem.slot.SinglePlayerSlot;
import net.minecraft.src.Minecraft;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/9/13
 */
public class SinglePlayerContainer extends HeliosContainer
{
    public HeliosSlot singlePlayerSlot;

    public SinglePlayerContainer(int id, float x, float y, float width, float height, HeliosMenu menu)
    {
        super(id, x, y, width, height, menu);
        singlePlayerSlot = new SinglePlayerSlot(this);
    }

    @Override
    public void drawContainerStuff(Minecraft mc, int x, int y)
    {
        singlePlayerSlot.drawSlot(x, y);
    }

    @Override
    protected void mouseClickedCheck(int par1, int par2, int par3)
    {
        singlePlayerSlot.mouseClicked(par1, par2, par3);
    }
}
