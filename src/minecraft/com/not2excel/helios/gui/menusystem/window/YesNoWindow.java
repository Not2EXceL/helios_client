package com.not2excel.helios.gui.menusystem.window;

import com.not2excel.helios.gui.menusystem.HeliosMenu;
import com.not2excel.helios.gui.menusystem.HeliosWindow;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/14/13
 */
public class YesNoWindow extends HeliosWindow
{
    private final String displayString;

    public YesNoWindow(HeliosMenu parentMenu, final String displayString)
    {
        super(parentMenu);
        this.displayString = displayString;
    }

    @Override
    protected void renderDrawing(float x, float y)
    {

    }

    @Override
    protected void checkMouseClicked(int par1, int par2, int par3)
    {

    }
}
