package com.not2excel.helios.gui.menusystem.menu;

import com.not2excel.helios.gui.menusystem.HeliosMenu;
import com.not2excel.helios.gui.menusystem.HeliosTabButton;
import com.not2excel.helios.gui.menusystem.container.SinglePlayerContainer;
import com.not2excel.helios.gui.menusystem.window.YesNoWindow;
import net.minecraft.src.*;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/9/13
 */
public class SinglePlayerMenu extends HeliosMenu
{
    /** set to true if you arein the process of deleteing a world/save */
    private boolean deleting;
    /** The delete button in the world selection GUI */
    private GuiButton buttonDelete;
    /** the select button in the world selection gui */
    private GuiButton buttonSelect;
    /** The rename button in the world selection GUI */
    private GuiButton buttonRename;
    private GuiButton buttonRecreate;
    /** True if a world has been selected. */
    private boolean selected;

    public SinglePlayerMenu(HeliosMenu parentMenu)
    {
        super(parentMenu);
    }

    @Override
    public void initGui()
    {
        initBaseMenuStuff();
        buttonList.add(this.buttonSelect = new HeliosTabButton(1, mc.displayWidth / 2 - tabWidth - 3,
                                           buttonList.size() * (tabHeight + 1) + 3, tabWidth,
                                           tabHeight, I18n.func_135053_a("selectWorld.select")));
        buttonList.add(new HeliosTabButton(3, mc.displayWidth / 2 - tabWidth - 3,
                                           buttonList.size() * (tabHeight + 1) + 3, tabWidth,
                                           tabHeight, I18n.func_135053_a("selectWorld.create")));
        buttonList.add(this.buttonRename = new HeliosTabButton(6, mc.displayWidth / 2 - tabWidth - 3,
                                           buttonList.size() * (tabHeight + 1) + 3, tabWidth,
                                           tabHeight, I18n.func_135053_a("selectWorld.rename")));
        buttonList.add(this.buttonDelete = new HeliosTabButton(2, mc.displayWidth / 2 - tabWidth - 3,
                                           buttonList.size() * (tabHeight + 1) + 3, tabWidth,
                                           tabHeight, I18n.func_135053_a("selectWorld.delete")));
        buttonList.add(this.buttonRecreate = new HeliosTabButton(7, mc.displayWidth / 2 - tabWidth - 3,
                                           buttonList.size() * (tabHeight + 1) + 3, tabWidth,
                                           tabHeight, I18n.func_135053_a("selectWorld.recreate")));
        buttonList.add(new HeliosTabButton(0, mc.displayWidth / 2 - tabWidth - 3,
                                           buttonList.size() * (tabHeight + 1) + 3, tabWidth,
                                           tabHeight, I18n.func_135053_a("gui.cancel")));
        this.buttonSelect.enabled = false;
        this.buttonDelete.enabled = false;
        this.buttonRename.enabled = false;
        this.buttonRecreate.enabled = false;
        container = new SinglePlayerContainer(0, startX, startY, mc.displayWidth / 2 - tabWidth - 3.5f,
                                              mc.displayHeight / 2 - 2, this);
    }

    @Override
    protected void actionPerformed(GuiButton button)
    {
        int selectedWorld = ((SinglePlayerContainer)container).singlePlayerSlot.selected;
        if (button.enabled)
        {
            if (button.id == 2)
            {
                String var2 = this.getSaveName(selectedWorld);

                if (var2 != null)
                {
                    this.deleting = true;
                    window = new YesNoWindow(this, "Testing the window system cuz yolo");
                    windowShown = true;
//                    GuiYesNo var3 = getDeleteWorldScreen(this, var2, selectedWorld);
//                    this.mc.displayGuiScreen(var3);
                }
            }
            else if (button.id == 1)
            {
                this.selectWorld(selectedWorld);
            }
            else if (button.id == 3)
            {
                this.mc.displayGuiScreen(new GuiCreateWorld(this));
            }
            else if (button.id == 6)
            {
                this.mc.displayGuiScreen(new GuiRenameWorld(this, this.getSaveFileName(selectedWorld)));
            }
            else if (button.id == 0)
            {
                this.mc.displayGuiScreen(this.parentMenu);
            }
            else if (button.id == 7)
            {
                GuiCreateWorld var5 = new GuiCreateWorld(this);
                ISaveHandler var6 = this.mc.getSaveLoader().getSaveLoader(this.getSaveFileName(selectedWorld), false);
                WorldInfo var4 = var6.loadWorldInfo();
                var6.flush();
                var5.func_82286_a(var4);
                this.mc.displayGuiScreen(var5);
            }
        }
    }

    public void selectWorld(int par1)
    {
        this.mc.displayGuiScreen((GuiScreen)null);

        if (!this.selected)
        {
            this.selected = true;
            String var2 = this.getSaveFileName(par1);

            if (var2 == null)
            {
                var2 = "World" + par1;
            }

            String var3 = this.getSaveName(par1);

            if (var3 == null)
            {
                var3 = "World" + par1;
            }

            if (this.mc.getSaveLoader().canLoadWorld(var2))
            {
                this.mc.launchIntegratedServer(var2, var3, (WorldSettings)null);
                this.mc.statFileWriter.readStat(StatList.loadWorldStat, 1);
            }
        }
    }

    /**
     * returns the file name of the specified save number
     */
    protected String getSaveFileName(int par1)
    {

        return ((SaveFormatComparator)this.saveList(par1)).getFileName();
    }

    /**
     * returns the name of the saved game
     */
    protected String getSaveName(int par1)
    {
        String var2 = ((SaveFormatComparator)this.saveList(par1)).getDisplayName();

        if (var2 == null || MathHelper.stringNullOrLengthZero(var2))
        {
            var2 = I18n.func_135053_a("selectWorld.world") + " " + (par1 + 1);
        }

        return var2;
    }

    private Object saveList(int par1)
    {
        return ((SinglePlayerContainer)container).singlePlayerSlot.list.get(par1);
    }

    public void confirmClicked(boolean par1, int par2)
    {
        if (this.deleting)
        {
            this.deleting = false;

            if (par1)
            {
                ISaveFormat var3 = this.mc.getSaveLoader();
                var3.flushCache();
                var3.deleteWorldDirectory(this.getSaveFileName(par2));

                try
                {
                    loadSaves();
                }
                catch (AnvilConverterException var5)
                {
                    var5.printStackTrace();
                }
            }

            this.mc.displayGuiScreen(this);
        }
    }

    /**
     * loads the saves
     */
    private void loadSaves() throws AnvilConverterException
    {
        ISaveFormat var1 = this.mc.getSaveLoader();
        ((SinglePlayerContainer)container).singlePlayerSlot.list = var1.getSaveList();
        Collections.sort(((SinglePlayerContainer)container).singlePlayerSlot.list);
        ((SinglePlayerContainer)container).singlePlayerSlot.selected = -1;
    }

    /**
     * Gets a GuiYesNo screen with the warning, buttons, etc.
     */
    public static GuiYesNo getDeleteWorldScreen(GuiScreen par0GuiScreen, String par1Str, int par2)
    {
        String var3 = I18n.func_135053_a("selectWorld.deleteQuestion");
        String var4 = "\'" + par1Str + "\' " + I18n.func_135053_a("selectWorld.deleteWarning");
        String var5 = I18n.func_135053_a("selectWorld.deleteButton");
        String var6 = I18n.func_135053_a("gui.cancel");
        GuiYesNo var7 = new GuiYesNo(par0GuiScreen, var3, var4, var5, var6, par2);
        return var7;
    }
    
    public void enableButtons()
    {
        this.buttonSelect.enabled = true;
        this.buttonDelete.enabled = true;
        this.buttonRename.enabled = true;
        this.buttonRecreate.enabled = true;   
    }
}
