package com.not2excel.helios.gui.screen;

import com.not2excel.helios.client.Variables;
import com.not2excel.helios.gui.screen.font.ScreenFontSelection;
import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiScreen;
import org.lwjgl.input.Keyboard;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/25/13
 */
public class ScreenHeliosOptions extends GuiScreen
{
    private GuiScreen parentScreen;

    public ScreenHeliosOptions(GuiScreen guiscreen)
    {
        parentScreen = guiscreen;
    }

    @Override
    public void initGui()
    {
        Keyboard.enableRepeatEvents(true);
        buttonList.clear();
        buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 - 12, "Font Selection"));
        buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 134, "Back"));
    }

    @Override
    protected void actionPerformed(GuiButton guibutton)
    {
        if (!guibutton.enabled)
        { return; }
        else if (guibutton.id == 0)
        { mc.displayGuiScreen(parentScreen); }
        else if (guibutton.id == 1)
        { mc.displayGuiScreen(new ScreenFontSelection(this)); }
    }

    @Override
    public void drawScreen(int i, int j, float f)
    {
        drawDefaultBackground();
        drawCenteredString(fontRenderer, Variables.getInstance().CLIENT_TITLE + " Options",
                           width / 2, (height / 4 - 60) + 20, 0xffffff);
        super.drawScreen(i, j, f);
    }
}
