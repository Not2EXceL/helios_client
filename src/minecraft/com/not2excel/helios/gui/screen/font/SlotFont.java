package com.not2excel.helios.gui.screen.font;

import net.minecraft.src.GuiSlot;
import net.minecraft.src.Minecraft;
import net.minecraft.src.Tessellator;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: steelers
 * Date: 7/4/13
 * Time: 10:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class SlotFont extends GuiSlot
{
    private ScreenFontList screenFontList;
    private int            selected;

    public SlotFont(Minecraft theMinecraft, ScreenFontList screenFontList)
    {
        super(theMinecraft, screenFontList.width, screenFontList.height, 32, screenFontList.height - 59, 14);
        this.screenFontList = screenFontList;
        this.selected = screenFontList.fontList.indexOf(screenFontList.currentFont);
        for (int i = 0; i < selected; i++)
        { this.amountScrolled += (float) (this.slotHeight); }
    }

    @Override
    protected int getContentHeight()
    {
        return this.getSize() * 14;
    }

    @Override
    protected int getSize()
    {
        return screenFontList.fontList.size();
    }

    @Override
    protected void elementClicked(int var1, boolean var2)
    {
        this.selected = var1;
    }

    @Override
    protected boolean isSelected(int var1)
    {
        return this.selected == var1;
    }

    @Override
    protected void drawBackground()
    {
    }

    @Override
    protected void drawSlot(int var1, int var2, int var3, int var4, Tessellator var5)
    {
        Font font = screenFontList.fontList.get(var1);
        screenFontList.drawString(screenFontList.getLocalFontRenderer(), font.getFontName(), var2, var3 + 1, 0xFFFFFF);
    }

    public int getSelected()
    {
        return this.selected;
    }
}