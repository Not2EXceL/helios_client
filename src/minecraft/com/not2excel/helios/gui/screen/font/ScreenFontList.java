package com.not2excel.helios.gui.screen.font;

import com.not2excel.helios.util.betterfonts.FontHandler;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiScreen;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: steelers
 * Date: 7/4/13
 * Time: 10:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class ScreenFontList extends GuiScreen
{
    private SlotFont slotFont;
    public final List<Font> fontList = new LinkedList<Font>();
    public GuiScreen parent;
    public Font      currentFont;

    public ScreenFontList(GuiScreen screen)
    {
        GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font[] fonts = e.getAllFonts();
        for (Font font : fonts)
        {
            fontList.add(font);
            if (FontHandler.getInstance().getFontName().equalsIgnoreCase(font.getFontName()))
            { currentFont = font; }
        }
        parent = screen;
    }

    public FontRenderer getLocalFontRenderer()
    {
        return this.fontRenderer;
    }

    @Override
    public void initGui()
    {
        buttonList.clear();
        buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 128, "Select Font"));
        buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 150, "Back"));
        slotFont = new SlotFont(this.mc, this);
        slotFont.registerScrollButtons(7, 8);
    }

    @Override
    public void actionPerformed(GuiButton button)
    {
        super.actionPerformed(button);
        if (button.id == 0)
        {
            if (slotFont.getSelected() == -1)
            { FontHandler.getInstance().setFontName(FontHandler.getInstance().getFontName()); }
            else
            { FontHandler.getInstance().setFontName(fontList.get(slotFont.getSelected()).getFontName()); }
            mc.displayGuiScreen(parent);
        }
        else if (button.id == 1)
        {
            mc.displayGuiScreen(parent);
        }
    }

    @Override
    protected void keyTyped(char c, int i)
    {
        if (i == Keyboard.KEY_ESCAPE)
        {
            mc.displayGuiScreen(parent);
        }
    }

    @Override
    public void drawScreen(int i, int j, float f)
    {
        drawDefaultBackground();
        slotFont.drawScreen(i, j, f);
        drawCenteredString(fontRenderer, "Select Font Name", width / 2, (height / 4 - 60) + 12, 0xffffff);
        super.drawScreen(i, j, f);
    }
}
