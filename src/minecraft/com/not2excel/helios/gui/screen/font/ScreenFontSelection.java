package com.not2excel.helios.gui.screen.font;

import com.not2excel.helios.logger.HeliosLogger;
import com.not2excel.helios.util.betterfonts.FontHandler;
import com.not2excel.helios.util.color.ColorHelper;
import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiTextField;
import org.lwjgl.input.Keyboard;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/25/13
 */
public class ScreenFontSelection extends GuiScreen
{
    public  GuiScreen    parent;
    private GuiTextField fontName;
    private GuiTextField fontSize;

    public ScreenFontSelection(GuiScreen screen)
    {
        parent = screen;
    }

    @Override
    public void updateScreen()
    {
        fontName.updateCursorCounter();
        fontSize.updateCursorCounter();
    }

    @Override
    public void initGui()
    {
        Keyboard.enableRepeatEvents(true);

        buttonList.clear();
        buttonList.add(new GuiButton(3, width / 2 - 100, height / 4 + 90, 98, 20, "Font List"));
        buttonList.add(new GuiButton(2, width / 2 + 2, height / 4 + 90, 98, 20,
                                     "Global TTF: " + (FontHandler.getInstance().isGlobalTTF() ?
                                                       ColorHelper.DARK_GREEN + "ON" :
                                                       ColorHelper.DARK_RED + "OFF")));
        buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 112, "Select Font"));
        buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 134, "Back"));

        fontName = new GuiTextField(fontRenderer, width / 2 - 100, height / 4 - 4, 200, 20);
        fontSize = new GuiTextField(fontRenderer, width / 2 - 100, height / 4 + 52, 200, 20);

        fontName.setMaxStringLength(228);
        fontSize.setMaxStringLength(228);

        fontName.setText(FontHandler.getInstance().getFontName());
        fontSize.setText("" + FontHandler.getInstance().getFontSize());
    }

    @Override
    public void actionPerformed(GuiButton button)
    {
        if (!button.enabled)
        { return; }
        if (button.id == 2)
        {
            try
            {
                FontHandler.getInstance().setGlobalTTF(!FontHandler.getInstance().isGlobalTTF());
                HeliosLogger.getInstance().logConsole("Font", String.format("GlobalTTF is %s",
                                                                            FontHandler.getInstance().isGlobalTTF() ?
                                                                            "ON" : "OFF"));
                FontHandler.getInstance().initializeFontRenderer();
                mc.displayGuiScreen(this);
            }
            catch (Exception err)
            { err.printStackTrace(); }
            button.displayString = "Global TTF: " + (FontHandler.getInstance().isGlobalTTF() ?
                                                     ColorHelper.DARK_GREEN + "ON" :
                                                     ColorHelper.DARK_RED + "OFF");
        }
        else if (button.id == 1)
        { mc.displayGuiScreen(parent); }
        else if (button.id == 3)
        { mc.displayGuiScreen(new ScreenFontList(this)); }
        else if (button.id == 0)
        {
            try
            {
                int size = Integer.parseInt(fontSize.getText());
                FontHandler.getInstance().setFontName(fontName.getText());
                FontHandler.getInstance().setFontSize(size);
                FontHandler.getInstance().initializeFontRenderer();
                mc.displayGuiScreen(this);
            }
            catch (Exception err)
            { err.printStackTrace(); }
        }
    }

    @Override
    protected void mouseClicked(int i, int j, int k)
    {
        super.mouseClicked(i, j, k);
        fontName.mouseClicked(i, j, k);
        fontSize.mouseClicked(i, j, k);
    }

    @Override
    protected void keyTyped(char c, int i)
    {
        if (c == '\r')
        { actionPerformed((GuiButton) buttonList.get(2)); }
        if (i == Keyboard.KEY_ESCAPE)
        { mc.displayGuiScreen(parent); }
        if (i == Keyboard.KEY_TAB)
        {
            if (fontName.isFocused())
            {
                fontName.setFocused(false);
                fontSize.setFocused(true);
            }
            else if (fontSize.isFocused())
            {
                fontName.setFocused(true);
                fontSize.setFocused(false);
            }
            if (!fontName.isFocused() && !fontSize.isFocused())
            { fontName.setFocused(true); }
        }
        fontName.textboxKeyTyped(c, i);
        fontSize.textboxKeyTyped(c, i);
    }

    @Override
    public void drawScreen(int i, int j, float f)
    {
        this.drawDefaultBackground();
        drawCenteredString(fontRenderer, "Font Selection",
                           width / 2, height / 4 - 50, 0xFFffffff);
        fontRenderer.drawStringWithShadow("Font Size:", width / 2 - 100, height / 4 + 40, 0xFFffffff);
        fontRenderer.drawStringWithShadow("Font Name:", width / 2 - 100, height / 4 - 16, 0xFFffffff);
        fontRenderer.drawStringWithShadow(
                "Current Font: " + ColorHelper.DARK_GREY + FontHandler.getInstance().getFontName(),
                width / 2 - 100, height / 4 - 26, 0xFFffffff);
        fontRenderer.drawStringWithShadow(
                "Current Size: " + ColorHelper.DARK_GREY + FontHandler.getInstance().getFontSize(),
                width / 2 - 100, height / 4 + 30, 0xFFffffff);
        fontSize.drawTextBox();
        fontName.drawTextBox();
        super.drawScreen(i, j, f);
    }
}
