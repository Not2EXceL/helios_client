package com.not2excel.lib.networking.misc;

import com.not2excel.helios.client.Wrapper;
import com.not2excel.helios.util.color.ColorHelper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/29/13
 */
public class ActionPVPKD
{
    private String player;
    private volatile Map<String, String> kdMap = new HashMap<String, String>();

    public ActionPVPKD()
    {
        player = Wrapper.getInstance().getMinecraft().session.func_111285_a();
        getKD();
    }

    public Map<String, String> getKdMap()
    {
        return kdMap;
    }

    public String getPlayer()
    {
        if (player.equalsIgnoreCase(Wrapper.getInstance().getMinecraft().session.func_111285_a()))
        { return "Your" + ColorHelper.WHITE; }
        return player + ColorHelper.WHITE + "'s";
    }

    public void setPlayer(final String name)
    {
        player = name;
    }

    public void getKD()
    {
        URL url;
        HttpURLConnection connection = null;
        InputStream is;
        String myCookie = "userId=" + player;
        try
        {
            url = new URL("http://actionpvp.com/users/" + player);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                                          "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setRequestProperty("User-Agent",
                                          "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, " +
                                          "like Gecko) Chrome/17.0.963.56 Safari/535.11");
            connection.setRequestProperty("Cookie", myCookie);
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            Thread.currentThread().sleep(2000l);
            if (connection.getResponseCode() >= 400)
            { is = connection.getErrorStream(); }
            else
            { is = connection.getInputStream(); }
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            String line2;
            StringBuffer response = new StringBuffer();
            while ((line = br.readLine()) != null)
            {
                if (line.toLowerCase().contains("kills"))
                {
                    line2 = br.readLine();
                    if (line2.contains("<td>"))
                    {
                        synchronized (kdMap)
                        { kdMap.put("kills", line2.replace("<td>", "").replace("</td>", "")); }
                    }
                }
                if (line.toLowerCase().contains("deaths"))
                {
                    line2 = br.readLine();
                    if (line2.contains("<td>"))
                    {
                        synchronized (kdMap)
                        { kdMap.put("deaths", line2.replace("<td>", "").replace("</td>", "")); }
                    }
                }
                if (line.toLowerCase().contains("kd ratio"))
                {
                    line2 = br.readLine();
                    if (line2.contains("<td>"))
                    {
                        synchronized (kdMap)
                        { kdMap.put("ratio", line2.replace("<td>", "").replace("</td>", "")); }
                    }
                }
            }
            br.close();
            return;
        }
        catch (Exception e)
        {
            System.out.println("Unable to fully create connection");
            e.printStackTrace();
            return;
        }
        finally
        {
            if (connection != null)
            { connection.disconnect(); }
        }
    }
}
