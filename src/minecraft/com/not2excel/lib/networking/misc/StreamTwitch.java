package com.not2excel.lib.networking.misc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/5/13
 */
public class StreamTwitch
{
    private String         channel;
    private URL            url;
    private BufferedReader reader;
    private boolean online = false;

    public StreamTwitch(final String channel)
    {
        this.channel = channel;
    }

    public synchronized void refreshStream()
    {
        try
        {
            if (channel.equals(""))
            { return; }
            url = new URL("http://api.justin.tv/api/stream/list.json?jsonp=&channel=" + channel);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            if (!reader.readLine().equals("[]"))
            { online = true; }
            else
            { online = false; }
        }
        catch (Exception e)
        { e.printStackTrace(); }
    }

    public URL getUrl()
    {
        return url;
    }

    public void setUrl(final URL url)
    {
        this.url = url;
    }

    public boolean isOnline()
    {
        return online;
    }

    public String getChannel()
    {
        return channel;
    }

    public void setChannel(final String channel)
    {
        this.channel = channel;
    }

    public static StreamTwitch getNewInstance(final String channel)
    {
        return new StreamTwitch(channel);
    }
}
