package com.not2excel.lib.logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class Logger<T>
{
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
    private Calendar         calendar         = Calendar.getInstance();

    public void logConsole(final String type, final T data)
    {
        System.out.println(String.format("[%s][%s]: %s", getCurrentTime(), type, data.toString()));
    }

    public void log(final T data)
    {
        logConsole("Info", data);
    }

    private String getCurrentTime()
    {
        return String.format("[%s]", simpleDateFormat.format(calendar.getTime()));
    }
}