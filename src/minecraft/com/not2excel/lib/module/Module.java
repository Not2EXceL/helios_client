package com.not2excel.lib.module;

import com.not2excel.lib.event.EventManager;
import com.not2excel.lib.event.FlexibleEventListener;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/21/13
 */
public class Module implements FlexibleEventListener
{
    private String label;
    private String author;
    private String description;
    private boolean status  = false;

    public Module(final String l)
    {
        this(l, null);
    }

    public Module(final String l, final String a)
    {
        this(l, a, null);
    }

    public Module(final String l, final String a, final String d)
    {
        this.label = l;
        this.author = a;
        this.description = d;
        addremoveListener(status);
    }

    public void onToggle(final boolean status){}

    public String getLabel()
    {
        return this.label;
    }

    public void setLabel(final String l)
    {
        this.label = l;
    }

    public String getAuthor()
    {
        return this.author;
    }

    public void setAuthor(final String a)
    {
        this.author = a;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String d)
    {
        this.description = d;
    }

    public boolean getStatus()
    {
        return status;
    }

    public void setStatus(final boolean b)
    {
        status = b;
        addremoveListener(b);
    }

    private void addremoveListener(final boolean status)
    {
        if (status)
        { EventManager.getInstance().registerListener(this); }
        else
        { EventManager.getInstance().unregisterListener(this); }
    }
}
