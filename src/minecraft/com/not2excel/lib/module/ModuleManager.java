package com.not2excel.lib.module;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/23/13
 */
public class ModuleManager
{
    private static volatile ModuleManager instance;
    private final HashMap<String, Module> moduleMap = new HashMap<String, Module>();

    public static ModuleManager getInstance()
    {
        if (instance == null)
        { instance = new ModuleManager(); }
        return instance;
    }

    public Module[] getModuleArray()
    {
        return moduleMap.values().toArray(new Module[moduleMap.values().size()]);
    }

    public void registerModule(final Module module)
    {
        synchronized (moduleMap)
        { this.moduleMap.put(module.getLabel(), module); }
    }

    public void unregisterModule(final String label)
    {
        synchronized (moduleMap)
        { this.moduleMap.remove(label); }
    }

    public Module getModule(final String label)
    {
        for (final Module module : this.getModuleArray())
        {
            if (module.getLabel().equalsIgnoreCase(label))
            { return module; }
        }
        return null;
    }
}
