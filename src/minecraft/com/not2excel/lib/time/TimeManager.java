package com.not2excel.lib.time;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/5/13
 */
public final class TimeManager
{
    private long lastTime;

    public synchronized void resetLastTime()
    {
        this.lastTime = System.nanoTime();
    }

    public synchronized boolean sleepMillis(final long time)
    {
        return sleep(time, TimeUnit.MILLISECONDS);
    }

    public synchronized boolean sleep(final long time, final TimeUnit timeUnit)
    {
        return timeUnit.convert(System.nanoTime() - lastTime, TimeUnit.NANOSECONDS) >= time;
    }
}
