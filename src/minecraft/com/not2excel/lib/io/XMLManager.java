package com.not2excel.lib.io;

import com.not2excel.lib.file.FileBase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/21/13
 */
public class XMLManager
{
    protected DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    protected FileBase fileBase;
    protected File     file;

    public XMLManager(final FileBase fileBase)
    {
        this.fileBase = fileBase;
        this.file = fileBase.getFile();
    }

    public synchronized NodeList parseXML(final String nodeName)
    {
        try
        {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document document = dBuilder.parse(file);
            document.getDocumentElement().normalize();
            return document.getElementsByTagName(nodeName);
        }
        catch (Exception e)
        { e.printStackTrace(); }
        return null;
    }

    public synchronized Document startGenerateXML()
    {
        try
        {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document document = dBuilder.newDocument();
            return document;
        }
        catch (ParserConfigurationException e)
        { e.printStackTrace(); }
        return null;
    }

    public synchronized Element generateRootElement(Document document)
    {
        Element rootElement = document.createElement(fileBase.getTrimmedLowercaseFileName());
        document.appendChild(rootElement);
        return rootElement;
    }

    public synchronized void finishGenerateXML(final Document document)
    {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try
        {
            transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        }
        catch (TransformerConfigurationException e)
        { e.printStackTrace(); }
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(file);
        StreamResult resultConsole = new StreamResult(System.out);
        try
        {
            transformer.transform(source, resultConsole);
            transformer.transform(source, result);
        }
        catch (TransformerException e)
        { e.printStackTrace(); }
    }
}
