package com.not2excel.lib.io;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/5/13
 */
public class BufferedIOManager implements IOManager
{
    protected final File           file;
    protected       BufferedReader bufferedReader;
    protected       BufferedWriter bufferedWriter;

    private BufferedIOManager(final File file)
    {
        this.file = file;
    }

    public static BufferedIOManager newInstance(final File file)
    {
        return new BufferedIOManager(file);
    }

    @Override
    public synchronized void setupReadStream()
    {
        try
        { bufferedReader = new BufferedReader(new FileReader(file)); }
        catch (final FileNotFoundException e)
        { e.printStackTrace(); }
    }

    @Override
    public synchronized void setupWriteStream()
    {
        try
        { bufferedWriter = new BufferedWriter(new FileWriter(file)); }
        catch (final IOException e)
        { e.printStackTrace(); }
    }

    @Override
    public synchronized void closeStream()
    {
        try
        {
            if (bufferedReader != null)
            { bufferedReader.close(); }

            if (bufferedWriter != null)
            { bufferedWriter.close(); }
        }
        catch (final IOException e)
        { e.printStackTrace(); }
    }

    protected synchronized String readLine()
    {
        try
        { return bufferedReader.readLine(); }
        catch (IOException e)
        { e.printStackTrace(); }
        return null;
    }

    protected synchronized void writeLine(final String line)
    {
        try
        {
            bufferedWriter.write(line);
            bufferedWriter.newLine();
        }
        catch (IOException e)
        { e.printStackTrace(); }
    }

    protected synchronized List<String> readFile()
    {
        final List<String> stringList = new LinkedList<String>();
        setupReadStream();
        String s;
        while((s = readLine()) != null)
        {
            if (!s.startsWith("##"))
            { stringList.add(s.trim()); }
        }
        closeStream();
        return stringList;
    }

    protected synchronized void writeFile(final List<String> stringList)
    {
        setupWriteStream();
        for (final String s : stringList)
        { writeLine(s); }
        closeStream();
    }
}
