package com.not2excel.lib.io;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/5/13
 */
public interface IOManager
{
    void setupReadStream();
    void setupWriteStream();
    void closeStream();
}
