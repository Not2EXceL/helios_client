package com.not2excel.lib.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/5/13
 */
public class URLManager implements IOManager
{
    private final URL            url;
    private       BufferedReader bufferedReader;

    public URLManager(final URL url)
    {
        this.url = url;
    }

    public static URLManager newInstance(final URL url)
    {
        return new URLManager(url);
    }

    @Override
    public synchronized void setupReadStream()
    {
        try
        { bufferedReader = new BufferedReader(new InputStreamReader(url.openStream())); }
        catch (final Exception e)
        { e.printStackTrace(); }
    }

    @Override
    public synchronized void setupWriteStream()
    {
        return;
    }

    @Override
    public synchronized void closeStream()
    {
        if (bufferedReader != null)
        {
            try
            { bufferedReader.close(); }
            catch (final IOException e)
            { e.printStackTrace(); }
        }
    }

    public synchronized String readLine()
    {
        try
        { return bufferedReader.readLine(); }
        catch (IOException e)
        { e.printStackTrace(); }
        return null;
    }
}