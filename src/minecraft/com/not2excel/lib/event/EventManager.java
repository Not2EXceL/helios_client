package com.not2excel.lib.event;

import java.lang.reflect.Method;
import java.util.*;

/**
 * Class to manage and fire events
 * Also keeps the caches of <code>RegisteredListener</code> and registered <code>Event</code>
 *
 * @author Richmond Steele
 * @since 8/24/13
 *        All rights Reserved
 *        Please read included LICENSE file
 */
public class EventManager
{
    private static volatile EventManager instance;
    private List<Class<? extends Event>>                      registeredEvents     =
            new ArrayList<Class<? extends Event>>();
    private Map<Class<? extends Event>, RegisteredListener[]> cachedEventListeners =
            new HashMap<Class<? extends Event>, RegisteredListener[]>();

    public static EventManager getInstance()
    {
        if (instance == null)
        {
            synchronized (EventManager.class)
            { instance = new EventManager(); }
        }
        return instance;
    }

    public void registerListener(final FlexibleEventListener listener)
    {
        final List<Class<? extends Event>> eventList = new LinkedList<Class<? extends Event>>();
        for (final Method method : listener.getClass().getDeclaredMethods())
        {
            final EventHandler handler = method.getAnnotation(EventHandler.class);
            if (handler == null)
            { continue; }
            eventList.add(handler.event());
        }
        mergeRegisteredEvents(eventList);
        final List<RegisteredListener> registeredListeners = new LinkedList<RegisteredListener>();
        for (final Class<? extends Event> eventClass : eventList)
        {
            final RegisteredListener registeredListener = new RegisteredListener(eventClass, listener);
            registeredListeners.add(registeredListener);
        }
        cacheRegisteredListener(registeredListeners);
    }

    public void unregisterListener(final FlexibleEventListener listener)
    {
        for (final Class<? extends Event> eventClass : registeredEvents)
        {
            synchronized (cachedEventListeners)
            {
                final List<RegisteredListener> registeredListeners = new LinkedList<RegisteredListener>();
                for (final RegisteredListener registeredListener : Arrays.asList(cachedEventListeners.get(eventClass)))
                {
                    if (!registeredListener.getListener().getClass().equals(listener.getClass()))
                    { registeredListeners.add(registeredListener); }
                }
                cachedEventListeners.remove(eventClass);
                cachedEventListeners.put(eventClass, registeredListeners.toArray(
                        new RegisteredListener[registeredListeners.size()]));
            }
        }
    }

    public void fireEvent(final Event event)
    {
        synchronized (cachedEventListeners)
        {
            if (cachedEventListeners.get(event.getClass()) == null ||
                cachedEventListeners.get(event.getClass()).length == 0)
            { return; }
            for (final RegisteredListener registeredListener : cachedEventListeners.get(event.getClass()))
            { registeredListener.fireEvent(event); }
        }
    }

    private void cacheRegisteredListener(final List<RegisteredListener> listenerList)
    {
        for (final Class<? extends Event> eventClass : registeredEvents)
        {
            if (!cachedEventListeners.containsKey(eventClass))
            {
                synchronized (cachedEventListeners)
                {
                    cachedEventListeners.put(eventClass, listenerList.toArray(
                            new RegisteredListener[listenerList.size()]));
                }
            }
            else
            {
                final List<RegisteredListener> registeredListeners = new LinkedList<RegisteredListener>();
                synchronized (cachedEventListeners)
                {
                    registeredListeners.addAll(Arrays.asList(cachedEventListeners.get(eventClass)));
                    for (final RegisteredListener listener : listenerList)
                    {
                        if (!registeredListeners.contains(listener))
                        { registeredListeners.add(listener); }
                    }
                    cachedEventListeners.remove(eventClass);
                    cachedEventListeners.put(eventClass, registeredListeners.toArray(
                            new RegisteredListener[registeredListeners.size()]));
                }
            }
        }
    }

    private void mergeRegisteredEvents(final List<Class<? extends Event>> eventList)
    {
        for (final Class<? extends Event> eventClass : eventList)
        {
            if (!registeredEvents.contains(eventClass))
            {
                synchronized (registeredEvents)
                { registeredEvents.add(eventClass); }
            }
        }
    }
}
