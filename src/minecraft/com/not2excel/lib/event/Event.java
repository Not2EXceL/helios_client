package com.not2excel.lib.event;

/**
 * Base event class
 *
 * @author Richmond Steele
 * @since 8/23/13
 *
 *        All rights Reserved
 *        Please read included LICENSE file
 */

public class Event
{
    /**
     * Name of event
     */
    protected final String name;

    /**
     * Constructor for event with class name
     */
    public Event()
    {
        this.name = this.getClass().getSimpleName();
    }

    /**
     * Constructor for event with custom name
     *
     * @param name
     */
    public Event(final String name)
    {
        this.name = name;
    }
}
