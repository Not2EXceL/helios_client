package com.not2excel.lib.event;

/**
 * Cancellable version of the event class
 *
 * @author Richmond Steele
 * @since 8/23/13
 *
 *        All rights Reserved
 *        Please read included LICENSE file
 */

public class CancellableEvent extends Event
{
    /**
     * Whether or not the Cancellable is cancelled
     */
    private boolean isCancelled = false;

    /**
     * Returns the state of isCancelled
     *
     * @return
     */
    public boolean isCancelled()
    {
        return this.isCancelled;
    }

    /**
     * Sets the state of the isCancelled
     *
     * @param isCancelled
     */
    public void setCancelled(boolean isCancelled)
    {
        this.isCancelled = isCancelled;
    }
}
