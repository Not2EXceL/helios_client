package com.not2excel.lib.event;

/**
 * Enum to prioritize events in which order to fire
 *
 * @author Richmond Steele
 * @since 8/23/13
 *
 *        All rights Reserved
 *        Please read included LICENSE file
 */

public enum EventPriority
{
    LOWEST,
    LOW,
    NORMAL,
    HIGH,
    HIGHEST;
}
