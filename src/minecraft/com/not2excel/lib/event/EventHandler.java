package com.not2excel.lib.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to mark methods that will fire with a relative event
 *
 * @author Richmond Steele
 * @since 8/23/13
 *
 *        All rights Reserved
 *        Please read included LICENSE file
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler
{
    /**
     * Determines which event the method
     * would be fired with.
     *
     * ie. EventTick, etc.
     *
     * @return
     */
    public Class<? extends Event> event();

    /**
     * Determines in what order this event will be called,
     * relative to other events.
     *
     * ie. HIGHEST events will fire before HIGH
     * NORMAL before LOW, etc.
     *
     * @return
     */
    public EventPriority priority() default EventPriority.NORMAL;
}
