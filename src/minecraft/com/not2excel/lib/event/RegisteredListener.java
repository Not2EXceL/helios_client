package com.not2excel.lib.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

/**
 * Object to hold the registered listener and all methods that contain
 * <code>@EventHandler</code>
 *
 * @author Richmond Steele
 * @since 8/23/13
 *
 *        All rights Reserved
 *        Please read included LICENSE file
 */

public class RegisteredListener
{
    //TODO: Finish everything

    private final Class<? extends Event> eventClass;
    private final FlexibleEventListener  listener;
    private final  Method[]               methods;

    public RegisteredListener(Class<? extends Event> eventClass, FlexibleEventListener listener)
    {
        this.eventClass = eventClass;
        this.listener = listener;
        final List<Method> methodList = new LinkedList<Method>();
        for (final Method method : listener.getClass().getDeclaredMethods())
        {
            final EventHandler handler = method.getAnnotation(EventHandler.class);
            if (handler == null)
            { continue; }
            if (this.eventClass.isAssignableFrom(handler.event()))
            { methodList.add(method); }
        }
        this.methods = methodList.toArray(new Method[methodList.size()]);
    }

    public Class<? extends Event> getEventClass()
    {
        return this.eventClass;
    }

    public FlexibleEventListener getListener()
    {
        return this.listener;
    }

    public void fireEvent(final Event event)
    {
        if (!event.getClass().equals(getEventClass()))
        { return; }
        for (final EventPriority priority : EventPriority.values())
        { fireEvent(event, priority); }
    }

    private void fireEvent(final Event event, final EventPriority priority)
    {
        for (final Method method : methods)
        {
            final EventHandler handler = method.getAnnotation(EventHandler.class);
            if (handler.priority() != priority)
            { continue; }
            try
            {
                if (method.getParameterTypes().length < 1)
                { method.invoke(getListener()); }
                else
                { method.invoke(getListener(), event); }
            }
            catch(IllegalAccessException e)
            { e.printStackTrace(); }
            catch (InvocationTargetException e)
            { e.printStackTrace(); }
        }
    }
}
