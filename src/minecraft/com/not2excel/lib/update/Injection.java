package com.not2excel.lib.update;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/5/13
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Injection
{
    String value();
    /**
     * eg. 1,1,1
     * #1 - 1 for injection, 2 for redefinition, 2 for removal
     * #2 - 0/1 for defining data
     * #3 - number of lines after
     *
     * idea borrowed from zachshelly's auto updater
     */
}
