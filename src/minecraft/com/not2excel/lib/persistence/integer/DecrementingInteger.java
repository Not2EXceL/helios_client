package com.not2excel.lib.persistence.integer;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/5/13
 */
public class DecrementingInteger
{
    private Random iRand     = new Random();
    private int    i         = 0;
    private int    increment = 5;

    public DecrementingInteger(final int i)
    {
        this.i = i;
    }

    public void setIncrement(final int e)
    {
        increment = e;
    }

    public void incrementInt()
    {
        i -= iRand.nextInt(increment * 2) - increment;
    }

    public int getInt()
    {
        return i;
    }
}
