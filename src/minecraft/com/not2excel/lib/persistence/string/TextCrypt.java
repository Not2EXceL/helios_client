package com.not2excel.lib.persistence.string;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: steelers
 * Date: 6/3/13
 * Time: 2:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class TextCrypt
{
    public static  String password = "";
    public static  String username = "";
    private static String seed     = "ChickenandWaffles";

    public static String encryptText(String input)
    {
        String midput = encryptPoly(input);
        String output = "";
        try
        { output = encryptAES(midput); }
        catch (Exception e)
        { e.printStackTrace(); }
        return output;
    }

    public static String decryptText(String input)
    {
        String midput = "";
        try
        { midput = decryptAES(input); }
        catch (Exception e)
        { e.printStackTrace(); }
        String output = decryptPoly(midput);
        return output;
    }

    public static String encryptAES(String cleartext) throws Exception
    {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] result = encryptAES(rawKey, cleartext.getBytes());
        return toHex(result);
    }

    public static String decryptAES(String encrypted) throws Exception
    {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] enc = toByte(encrypted);
        byte[] result = decryptAES(rawKey, enc);
        return new String(result);
    }

    private static byte[] getRawKey(byte[] seed) throws Exception
    {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(seed);
        kgen.init(128, sr);
        SecretKey skey = kgen.generateKey();
        byte[] raw = skey.getEncoded();
        return raw;
    }

    private static byte[] encryptAES(byte[] raw, byte[] clear) throws Exception
    {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }

    private static byte[] decryptAES(byte[] raw, byte[] encrypted) throws Exception
    {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    public static byte[] toByte(String hexString)
    {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
        { result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue(); }
        return result;
    }

    public static String toHex(byte[] buf)
    {
        if (buf == null)
        { return ""; }
        StringBuffer result = new StringBuffer(2 * buf.length);
        for (int i = 0; i < buf.length; i++)
        { appendHex(result, buf[i]); }
        return result.toString();
    }

    private final static String HEX = "0123456789ABCDEF";

    private static void appendHex(StringBuffer sb, byte b)
    {
        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
    }

    public static String encryptPoly(String input)
    {
        String output = "";
        Random rand = new Random();
        int O = rand.nextInt(99 - 10) + 10;
        char[] CA = input.toCharArray();
        for (char c : CA)
        {
            try
            {
                int asc = (int) c;
                int enc = asc + O;
                output = output + enc + ":";
            }
            catch (Exception e)
            { e.printStackTrace(); }
        }
        return O + ":" + output;
    }

    public static String decryptPoly(String input)
    {
        String output = "";
        String[] SA = input.split(":");
        for (String c : SA)
        {
            try
            {
                int epic = Integer.parseInt(c) - Integer.parseInt(SA[0]);
                output = output + (char) epic;
            }
            catch (Exception e)
            { e.printStackTrace(); }
        }
        return output.substring(1);
    }
}
