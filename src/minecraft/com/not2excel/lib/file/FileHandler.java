package com.not2excel.lib.file;

import com.not2excel.helios.file.FontFile;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/21/13
 */
public class FileHandler
{
    private static volatile FileHandler instance;
    private final List<FileBase> fileList = new LinkedList<FileBase>();

    public static FileHandler getInstance()
    {
        if (instance == null)
        { instance = new FileHandler(); }
        return instance;
    }

    public void loadFiles()
    {
        addFile(new FontFile());
    }

    private void addFile(FileBase file)
    {
        synchronized (fileList)
        { fileList.add(file); }
    }

    public FileBase[] getFileArray()
    {
        return fileList.toArray(new FileBase[fileList.size()]);
    }

    public FileBase getFile(String name)
    {
        for (FileBase fileBase : getFileArray())
        {
            if (fileBase.getFileName().equalsIgnoreCase(name))
            { return fileBase; }
        }
        return null;
    }
}
