package com.not2excel.lib.file;

import com.not2excel.helios.client.Variables;
import com.not2excel.helios.client.Wrapper;
import com.not2excel.lib.event.EventManager;
import com.not2excel.lib.event.FlexibleEventListener;
import com.not2excel.lib.io.XMLManager;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 8/21/13
 */
public abstract class FileBase implements FlexibleEventListener
{
    protected File   fileLocation = new File(Wrapper.getInstance().getMinecraft().mcDataDir,
                                             Variables.getInstance().CLIENT_TITLE);
    protected String nameSuffix   = ".xml";
    protected XMLManager xmlManager;
    private   File       fileBase;
    private   String     trueFileName;

    public FileBase(String name)
    {
        initFile(name);
    }

    protected void initFile(final String name)
    {
        trueFileName = name;
        String fileName = name + nameSuffix;
        fileBase = new File(fileLocation, fileName);
        xmlManager = new XMLManager(this);
        if (!fileBase.exists())
        {
            try
            { fileBase.createNewFile(); }
            catch (IOException e)
            { e.printStackTrace(); }
        }
        EventManager.getInstance().registerListener(this);
    }

    public String getFileName()
    {
        return trueFileName;
    }

    public String getTrimmedLowercaseFileName()
    {
        return trueFileName.toLowerCase().trim();
    }

    public File getFile()
    {
        return fileBase;
    }

    public abstract void loadFile();

    public abstract void saveFile();


}
