package com.not2excel.lib.command;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/21/13
 */
public interface Command
{
    void onCommand(final String fullCommand, final String[] args);
}
