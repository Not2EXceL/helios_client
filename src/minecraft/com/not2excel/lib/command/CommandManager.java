package com.not2excel.lib.command;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public class CommandManager
{
    private static volatile CommandManager instance;
    private final List<CommandBase> commandList = new LinkedList<CommandBase>();

    public static CommandManager getInstance()
    {
        if (instance == null)
        { instance = new CommandManager(); }
        return instance;
    }

    public List<CommandBase> getCommandList()
    {
        return Collections.unmodifiableList(commandList);
    }

    public void registerCommand(CommandBase command)
    {
        synchronized (commandList)
        { commandList.add(command); }
    }

    public void unregisterCommand(CommandBase command)
    {
        synchronized (commandList)
        { commandList.remove(command); }
    }

    public void runCommands(final String input)
    {
        for (CommandBase command : getCommandList())
        {
            if (command.getCommand().equalsIgnoreCase(input))
            { command.onCommand(); }
        }
    }
}
