package com.not2excel.lib.command;

/**
 * Created with IntelliJ IDEA.
 *
 * @author steelers
 * @since 7/24/13
 */
public abstract class CommandBase implements Command
{
    private String command;
    private String description;
    private String syntax;

    public CommandBase(final String command, final String description, final String syntax)
    {
        this.command = command;
        this.description = description;
        this.syntax = syntax;
    }

    public String getCommand()
    {
        return command;
    }

    public String getDescription()
    {
        return description;
    }

    public String getSyntax()
    {
        return syntax;
    }

    public void onCommand(){}
}
